function warehouse() {
    sceneLocation = "warehouse";
    currentBackground = warehouseImage[1];
    updateMap(mapImage[11]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(warehouseImage[1]);
    scene.addChild(warehouseImage[6]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(warehouseImage[0], -990, -980);
}

function talkNemoA() {
    removeListeners();
    warehouseImage[6].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(warehouseImage[7]);
    warehouseImage[7].alpha = 0;
    
    resetSubtitle(19);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(warehouseImage[7], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(warehouseImage[6], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (nemoTalked == true) {
                                  warehouseImage[1].addEventListener("click", back, false);
                              } 
                              scene.removeChild(warehouseImage[6]);
                              playDialogue("NemoA");
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {warehouseImage[1].removeAllEventListeners();
                                                            scene.removeChild(subtitleBox);
                                                            rolloverBox.alpha = 0.8;
                                                            warehouseImage[7].addEventListener("mouseover", overWarehouseItem, false);
                                                            warehouseImage[7].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
                                                            warehouseImage[7].addEventListener("click", clickWarehouseItem, false);});});
    
    function back() {
        warehouseImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(warehouseImage[6]);
        warehouseImage[6].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(warehouseImage[6], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[7], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(warehouseImage[7]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function talkNemoB() {
    soundInstance = createjs.Sound.play("NemoTransform");
    
    removeListeners();
    warehouseImage[7].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    resetSubtitle(20);
    
    transform();
    
    function transform() {
        scene.addChild(warehouseImage[3]);
        scene.addChild(warehouseImage[4]);
        scene.addChild(warehouseImage[5]);
        scene.addChild(warehouseImage[8]);
        
        warehouseImage[3].alpha = 0;
        warehouseImage[4].alpha = 0;
        warehouseImage[5].alpha = 0;
        warehouseImage[8].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(warehouseImage[7], {loop: false}).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[3], {loop: false}).to({alpha:1}, 1000, createjs.Ease.quadInOut).wait(1000).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[4], {loop: false}).wait(1000).to({alpha:1}, 1000, createjs.Ease.quadInOut).wait(1000).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[5], {loop: false}).wait(2000).to({alpha:1}, 1000, createjs.Ease.quadInOut).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[8], {loop: false}).wait(3000).to({alpha:1}, 1000, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(warehouseImage[7]);
                              playDialogue("NemoB");
                              scene.addChild(subtitleBox);
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {scene.removeChild(subtitleBox);
                                                            rolloverBox.alpha = 0.8;
                                                            warehouseImage[8].addEventListener("mouseover", overWarehouseItem, false);
                                                            warehouseImage[8].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
                                                            warehouseImage[8].addEventListener("click", clickWarehouseItem, false);});});
    }
}

function talkNemoC() {
    soundInstance = createjs.Sound.play("NemoTransform");
    
    removeListeners();
    warehouseImage[8].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    resetSubtitle(21);
    
    transform();
    
    function transform() {
        scene.addChild(warehouseImage[5]);
        scene.addChild(warehouseImage[4]);
        scene.addChild(warehouseImage[3]);
        scene.addChild(warehouseImage[2]);
        
        warehouseImage[5].alpha = 0;
        warehouseImage[4].alpha = 0;
        warehouseImage[3].alpha = 0;
        warehouseImage[2].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(warehouseImage[8], {loop: false}).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[5], {loop: false}).to({alpha:1}, 1000, createjs.Ease.quadInOut).wait(1000).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[4], {loop: false}).wait(1000).to({alpha:1}, 1000, createjs.Ease.quadInOut).wait(1000).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[3], {loop: false}).wait(2000).to({alpha:1}, 1000, createjs.Ease.quadInOut).to({alpha:0}, 1000, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[2], {loop: false}).wait(3000).to({alpha:1}, 1000, createjs.Ease.quadInOut)
            .call(function() {if (nemoTalked == true) {
                                  warehouseImage[1].addEventListener("click", function() {back(false);}, false);
                              } else {
                                  nemoTalked = true;
                              }
                              scene.removeChild(warehouseImage[8]);
                              playDialogue("NemoC");
                              scene.addChild(subtitleBox);
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {back(true);});});
    }
    
    function back(done) {
        if (done == true && familyAccess == false) {
            familyAccess = true;
            newLocation(mapImage[12]);
            newLocation(mapImage[13]);
        }
        
        warehouseImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.removeChild(warehouseImage[3]);
        scene.removeChild(warehouseImage[4]);
        scene.removeChild(warehouseImage[5]);
        
        scene.addChild(warehouseImage[6]);
        warehouseImage[6].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(warehouseImage[6], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(warehouseImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(warehouseImage[2]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overWarehouseItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 6:
            rollover.text = "Ask Nemo who she is";
            break; 
        case 7:
            rollover.text = "Ask Nemo to prove she is alien";
            break; 
        case 8:
            rollover.text = "Ask Nemo why she is revealing this";
            break; 
    }
    addShadow(event, "#222222");
}

function clickWarehouseItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 6:
            talkNemoA();
            break; 
        case 7:
            talkNemoB();
            break; 
        case 8:
            talkNemoC();
            break; 
    }
}

function addWarehouseListeners() {
    for (var i = 6; i < fileData[24].numFile; i++) {
        warehouseImage[i].addEventListener("mouseover", overWarehouseItem, false);
        warehouseImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        warehouseImage[i].addEventListener("click", clickWarehouseItem, false);
    }
    addIconListeners();
}

function removeWarehouseListeners() {
    for (var i = 6; i < fileData[24].numFile; i++) {
        warehouseImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}