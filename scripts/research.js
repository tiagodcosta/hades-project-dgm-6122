function setupResearch() {
    research = new createjs.Container();
    
    research.addChild(researchImage[0]);
    research.addChild(researchImage[1]);
    research.addChild(researchImage[4]);
    research.addChild(researchImage[5]);
    
    researchImage[0].addEventListener("click", function() {}, false);
    
    for (var i = 4; i < fileData[10].numFile; i++) {
        researchImage[i].addEventListener("mouseover", function(event) {addShadow(event, "#222222");}, false);
        researchImage[i].addEventListener("mouseout", removeShadow, false);
        researchImage[i].addEventListener("click", clickResearch, false);
    }
}

function timeline() {
    research.addChild(researchImage[1]);
    research.addChild(researchImage[4]);
    research.addChild(researchImage[5]);
    
    research.removeChild(researchImage[2]);
    research.removeChild(researchImage[3]);
    research.removeChild(researchImage[6]);
    
    stage.update();
}

function viewArticle(num) {
    research.addChild(researchImage[num+2]);
    research.addChild(researchImage[6]);
    
    research.removeChild(researchImage[1]);
    research.removeChild(researchImage[4]);
    research.removeChild(researchImage[5]);
    
    research.addChild(tabletImage[15]);
    
    stage.update();
}

function clickResearch(event) {
    var id = event.currentTarget.id;
    
    switch(id) {
        case 4:
            viewArticle(0);
            break;
        case 5:
            viewArticle(1);
            break;
        case 6:
            timeline();
            break;
    }
}