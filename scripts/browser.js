function setupBrowser() {
    browser = new createjs.Container();
    
    browser.addChild(browserImage[0]);
    
    browserImage[0].addEventListener("click", function() {}, false);
    
    for (var i = 6; i < fileData[5].numFile; i++) {
        browserImage[i].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
        browserImage[i].addEventListener("mouseout", removeShadow, false);
        browserImage[i].addEventListener("click", clickBrowser, false);
    }
    
    browser.addChild(browserImage[1]);
    browser.addChild(browserImage[6]);
    
    // 0 - Search page, 1 - Results page, 2 - Article pages
    browserPage = 0;
}

function searchPage() {
    browserPage = 0;
    
    browser.addChild(browserImage[1]);
    browser.addChild(browserImage[6]);
    
    browser.removeChild(browserImage[2]);
    browser.removeChild(browserImage[7]);
    browser.removeChild(browserImage[8]);
    browser.removeChild(browserImage[9]);
    browser.removeChild(browserImage[10]);
}

function resultsPage() {
    browserPage = 1;
    
    browser.addChild(browserImage[2]);
    browser.addChild(browserImage[7]);
    browser.addChild(browserImage[8]);
    browser.addChild(browserImage[9]);
    browser.addChild(browserImage[10]);

    browser.removeChild(browserImage[1]);
    browser.removeChild(browserImage[3]);
    browser.removeChild(browserImage[4]);
    browser.removeChild(browserImage[5]);
    browser.removeChild(browserImage[6]);
}

function articlePage(num) {
    browserPage = 2;
    
    browser.addChild(browserImage[3+num]);
    
    browser.removeChild(browserImage[2]);
    browser.removeChild(browserImage[8]);
    browser.removeChild(browserImage[9]);
    browser.removeChild(browserImage[10]);  
} 

function clickBrowser(event) {
    var id = event.currentTarget.id;
    
    switch(id) {
        case 6:
            resultsPage();
            break;
        case 7:
            if (browserPage == 1) {
                searchPage();
            } else {
                resultsPage();
            }
            break;
        case 8:
            articlePage(0);
            break;
        case 9:
            articlePage(1);
            break;
        case 10:
            articlePage(2);
            break;   
    }
    
    stage.update();
}