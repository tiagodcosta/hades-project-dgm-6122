function auntHouse() {
    sceneLocation = "auntHouse";
    currentBackground = auntImage[1];
    updateMap(mapImage[12]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(auntImage[1]);
    scene.addChild(auntImage[3]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(auntImage[0], -420, -1600);
}

function talkAunt() {
    removeListeners();
    auntImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(auntImage[2]);
    warehouseImage[2].alpha = 0;
    
    resetSubtitle(22);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(auntImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(auntImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (auntTalked == true) {
                                  auntImage[1].addEventListener("click", back, false);
                              } else {
                                  auntTalked = true;
                              }
                              scene.removeChild(auntImage[6]);
                              playDialogue("Aunt");
                              scene.addChild(subtitle);
                              changeSubtitle(0, back);});
    
    function back() {
        auntImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(auntImage[3]);
        auntImage[3].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(auntImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(auntImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(auntImage[2]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overAuntHouseItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            rollover.text = "Ask Aunt Betty about Uncle Aaron";
            break; 
    }
    addShadow(event, "#222222");
}

function clickAuntHouseItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            talkAunt();
            break; 
    }
}

function addAuntHouseListeners() {
    for (var i = 3; i < fileData[25].numFile; i++) {
        auntImage[i].addEventListener("mouseover", overAuntHouseItem, false);
        auntImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        auntImage[i].addEventListener("click", clickAuntHouseItem, false);
    }
    addIconListeners();
}

function removeAuntHouseListeners() {
    for (var i = 3; i < fileData[25].numFile; i++) {
        auntImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}