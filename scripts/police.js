function police() {
    sceneLocation = "police";
    currentBackground = policeImage[1];
    updateMap(mapImage[8]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(policeImage[1]);
    scene.addChild(policeImage[5]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(policeImage[0], -1205, -1660);
}

function talkPolice() {
    removeListeners();
    policeImage[5].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    var usbHanded = false;
    
    scene.addChild(policeImage[2]);
    policeImage[2].alpha = 0;
    
    resetSubtitle(15);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(policeImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(policeImage[5], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (policeTalked == true) {
                                  policeImage[1].addEventListener("click", back, false);
                              } else {
                                  policeTalked = true;
                              }
                              scene.removeChild(policeImage[5]);
                              playDialogue("Police");
                              scene.addChild(subtitle);
                              if (criminalAccess == true) {
                                  changeSubtitle(0, back);
                              } else {
                                  changeSubtitle(0, handUSB);
                              }});
    
    function handUSB() {
        if (prisonAccess == false) {
            prisonAccess = true;
            newLocation(mapImage[10]);
        }
        
        usbHanded = true;
        
        scene.addChild(policeImage[3]);
        scene.addChild(policeImage[6]);
        policeImage[3].alpha = 0;
        policeImage[6].alpha = 0;
        
        scene.removeChild(subtitleBox);
        
        createjs.Tween.get(policeImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(policeImage[6], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(policeImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance = createjs.Sound.play("HandObject");
                              scene.removeChild(policeImage[2]);
                              rolloverBox.alpha = 0.8;
                              policeImage[6].addEventListener("mouseover", overPoliceItem, false);
                              policeImage[6].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
                              policeImage[6].addEventListener("click", clickPoliceItem, false);
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
    
    function back() {
        policeImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(policeImage[5]);
        policeImage[5].alpha = 0;
        
        if(usbHanded == true) {
            var num = 3;
            scene.removeChild(policeImage[6]);
        } else {
            var num = 2;
        }
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(policeImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(policeImage[num], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(policeImage[num]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function viewCriminalUSB() {
    soundInstance = createjs.Sound.play("ViewObject", loopSound);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    policeImage[1].removeAllEventListeners();
    policeImage[1].addEventListener("click", back, false);
    
    scene.addChild(policeImage[5]);
    policeImage[5].alpha = 0;
    
    scene.addChild(greyLayer);
    greyLayer.alpha = 0;
    
    scene.addChild(policeImage[4]);
    policeImage[4].x = 640;
    policeImage[4].y = 400;
    policeImage[4].scaleX = 1/5;
    policeImage[4].scaleY = 1/5;

    scene.removeChild(policeImage[6]);

    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(policeImage[4], {loop: false}).to({x:300, y:285, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(policeImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0.7}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(policeImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
        .call(function() {scene.removeChild(policeImage[3]);
                          createjs.Ticker.removeEventListener("tick", stage);});
    
    function back() {
        policeImage[1].removeAllEventListeners();
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(policeImage[4], {loop: false}).to({x:40, y:55, scaleX:1/5, scaleY:1/5}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance.stop();
                              scene.removeChild(greyLayer);
                              scene.removeChild(policeImage[4]);
                              criminalAccess = true;
                              newApp(2);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overPoliceItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 5:
            rollover.text = "Ask policewoman about HadeS";
            break; 
        case 6:
            rollover.text = "Take flash drive";
            break;
    }
    addShadow(event, "#222222");
}

function clickPoliceItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 5:
            talkPolice();
            break; 
        case 6:
            viewCriminalUSB();
    }
}

function addPoliceListeners() {
    for (var i = 5; i < fileData[21].numFile; i++) {
        policeImage[i].addEventListener("mouseover", overPoliceItem, false);
        policeImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        policeImage[i].addEventListener("click", clickPoliceItem, false);
    }
    addIconListeners();
}

function removePoliceListeners() {
    for (var i = 5; i < fileData[21].numFile; i++) {
        policeImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}