function setupCriminal() {
    criminal = new createjs.Container();
    
    criminal.addChild(criminalImage[0]);
    criminal.addChild(criminalImage[1]);
    criminal.addChild(criminalImage[2]);
    
    criminalImage[0].addEventListener("click", function() {}, false);
    
    criminalImage[2].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
    criminalImage[2].addEventListener("mouseout", removeShadow, false);
    criminalImage[2].addEventListener("click", showCriminalGraphs, false);
}

function showCriminalGraphs() {
    nemoCount++;
    
    addNote(9); 
    
    criminal.addChild(criminalImage[3]);
    
    criminal.removeChild(criminalImage[1]);
    criminal.removeChild(criminalImage[2]);
    
    stage.update();
}