function prison() {
    sceneLocation = "prison";
    currentBackground = prisonImage[1];
    updateMap(mapImage[10]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(prisonImage[1]);
    scene.addChild(prisonImage[4]);
    scene.addChild(prisonImage[5]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(prisonImage[0], -1330, -375);
}

 function talkPrisonerA() {
     removeListeners();
     prisonImage[4].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
     rollover.text = "";
     rolloverBox.alpha = 0.5;
    
     scene.addChild(prisonImage[2]);
     prisonImage[2].alpha = 0;
    
     resetSubtitle(17);
     scene.addChild(subtitleBox);
    
     createjs.Ticker.addEventListener("tick", stage); 
     createjs.Tween.get(prisonImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
     createjs.Tween.get(prisonImage[4], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
             .call(function() {if (prisonerATalked == true) {
                                   prisonImage[1].addEventListener("click", back, false);
                               } else {
                                   prisonerATalked = true;
                               }
                               scene.removeChild(prisonImage[4]);
                               playDialogue("PrisonerA");
                               scene.addChild(subtitle);
                               changeSubtitle(0, back);});
    
     function back() {
         prisonImage[1].removeAllEventListeners();
         createjs.Tween.removeAllTweens();
         soundInstance.stop();
         scene.removeChild(subtitle);
         scene.removeChild(subtitleBox);
        
         scene.addChild(prisonImage[4]);
         prisonImage[4].alpha = 0;
        
         createjs.Ticker.addEventListener("tick", stage); 
         createjs.Tween.get(prisonImage[4], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
         createjs.Tween.get(prisonImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
             .call(function() {scene.removeChild(prisonImage[2]);
                               rolloverBox.alpha = 0.8;
                               addListeners();
                               createjs.Ticker.removeEventListener("tick", stage);});
     }
 }
 function talkPrisonerB() {
     removeListeners();
     prisonImage[5].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
     rollover.text = "";
     rolloverBox.alpha = 0.5;
    
     scene.addChild(prisonImage[3]);
     prisonImage[3].alpha = 0;
    
     resetSubtitle(18);
     scene.addChild(subtitleBox);
    
     createjs.Ticker.addEventListener("tick", stage); 
     createjs.Tween.get(prisonImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
     createjs.Tween.get(prisonImage[5], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
             .call(function() {if (prisonerBTalked == true) {
                                   prisonImage[1].addEventListener("click", back, false);
                               } else {
                                   prisonerBTalked = true;
                               }
                               scene.removeChild(prisonImage[5]);
                               playDialogue("PrisonerB");
                               scene.addChild(subtitle);
                               changeSubtitle(0, back);});
    
     function back() {
         prisonImage[1].removeAllEventListeners();
         createjs.Tween.removeAllTweens();
         soundInstance.stop();
         scene.removeChild(subtitle);
         scene.removeChild(subtitleBox);
        
         scene.addChild(prisonImage[5]);
         prisonImage[5].alpha = 0;
        
         createjs.Ticker.addEventListener("tick", stage); 
         createjs.Tween.get(prisonImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
         createjs.Tween.get(prisonImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
             .call(function() {scene.removeChild(prisonImage[3]);
                               rolloverBox.alpha = 0.8;
                               addListeners();
                               createjs.Ticker.removeEventListener("tick", stage);});
     }
 }

function overPrisonItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 4:
            rollover.text = "Talk to prisoner";
            break;
        case 5:
            rollover.text = "Talk to prisoner";
            break;      
    }
    addShadow(event, "#222222");
}

function clickPrisonItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 4:
            talkPrisonerA();
            break; 
        case 5:
            talkPrisonerB();
            break; 
    }
}

function addPrisonListeners() {
    for (var i = 4; i < fileData[23].numFile; i++) {
        prisonImage[i].addEventListener("mouseover", overPrisonItem, false);
        prisonImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        prisonImage[i].addEventListener("click", clickPrisonItem, false);
    }
    addIconListeners();
}

function removePrisonListeners() {
    for (var i = 4; i < fileData[23].numFile; i++) {
        prisonImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}