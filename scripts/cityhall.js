function cityHall() {
    sceneLocation = "cityHall";
    currentBackground = cityHallImage[1];
    updateMap(mapImage[7]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(cityHallImage[1]);
    scene.addChild(cityHallImage[6]);
    scene.addChild(cityHallImage[7]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(cityHallImage[0], -1230, -1730);
}

function talkMayor() {
    removeListeners();
    cityHallImage[6].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(cityHallImage[2]);
    cityHallImage[2].alpha = 0;
    
    resetSubtitle(13);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(cityHallImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(cityHallImage[6], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (mayorTalked == true) {
                                  cityHallImage[1].addEventListener("click", function() {back(false);}, false);
                              } else {
                                  mayorTalked = true;
                              }
                              scene.removeChild(cityHallImage[6]);
                              playDialogue("Mayor");
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {back(true);});});
    
    function back(done) {
        if (done == true && noteAccess[4] == false) {
            addNote(4);
            addNote(5);
        }
        
        cityHallImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(cityHallImage[6]);
        cityHallImage[6].alpha = 0;

        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(cityHallImage[6], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(cityHallImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(cityHallImage[2]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function talkCityHallAdmin() {
    removeListeners();
    cityHallImage[7].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    var usbHanded = false;
    
    scene.addChild(cityHallImage[3]);
    cityHallImage[3].alpha = 0;
    
    resetSubtitle(14);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(cityHallImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(cityHallImage[7], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (cityAdminTalked == true) {
                                  cityHallImage[1].addEventListener("click", back, false);
                              } else {
                                  cityAdminTalked = true;
                              }
                              scene.removeChild(cityHallImage[7]);
                              playDialogue("CityHallAdmin");
                              scene.addChild(subtitle);
                              if (biodataAccess == true) {
                                  changeSubtitle(0, back);
                              } else {
                                  changeSubtitle(0, handUSB);
                              }});
    
    function handUSB() {
        usbHanded = true;
        
        scene.addChild(cityHallImage[4]);
        scene.addChild(cityHallImage[8]);
        cityHallImage[4].alpha = 0;
        cityHallImage[8].alpha = 0;
        
        scene.removeChild(subtitleBox);
        
        createjs.Tween.get(cityHallImage[4], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(cityHallImage[8], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(cityHallImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance = createjs.Sound.play("HandObject");
                              scene.removeChild(cityHallImage[3]);
                              rolloverBox.alpha = 0.8;
                              cityHallImage[8].addEventListener("mouseover", overCityHallItem, false);
                              cityHallImage[8].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
                              cityHallImage[8].addEventListener("click", clickCityHallItem, false);
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
    
    function back() {
        cityHallImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(cityHallImage[7]);
        cityHallImage[7].alpha = 0;
        
        if(usbHanded == true) {
            var num = 4;
            scene.removeChild(cityHallImage[8]);
        } else {
            var num = 3;
        }
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(cityHallImage[7], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(cityHallImage[num], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(cityHallImage[num]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function viewBiodataUSB() {
    soundInstance = createjs.Sound.play("ViewObject", loopSound);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    cityHallImage[1].removeAllEventListeners();
    cityHallImage[1].addEventListener("click", back, false);
    
    scene.addChild(cityHallImage[7]);
    cityHallImage[7].alpha = 0;
    
    scene.addChild(greyLayer);
    greyLayer.alpha = 0;
    
    scene.addChild(cityHallImage[5]);
    cityHallImage[5].x = 585;
    cityHallImage[5].y = 375;
    cityHallImage[5].scaleX = 1/5;
    cityHallImage[5].scaleY = 1/5;

    scene.removeChild(cityHallImage[8]);

    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(cityHallImage[5], {loop: false}).to({x:300, y:285, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(cityHallImage[7], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0.7}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(cityHallImage[4], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
        .call(function() {scene.removeChild(cityHallImage[4]);
                          createjs.Ticker.removeEventListener("tick", stage);});
    
    function back() {
        cityHallImage[1].removeAllEventListeners();
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(cityHallImage[5], {loop: false}).to({x:40, y:55, scaleX:1/5, scaleY:1/5}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance.stop();
                              scene.removeChild(greyLayer);
                              scene.removeChild(cityHallImage[5]);
                              biodataAccess = true;
                              newApp(1);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overCityHallItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 6:
            rollover.text = "Show mayor card from nurse";
            break; 
        case 7:
            rollover.text = "Ask admin for citizen records";
            break;
        case 8:
            rollover.text = "Take flash drive";
            break;
    }
    addShadow(event, "#222222");
}

function clickCityHallItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 6:
            talkMayor();
            break; 
        case 7:
            talkCityHallAdmin();
            break;
        case 8:
            viewBiodataUSB();
    }
}

function addCityHallListeners() {
    for (var i = 6; i < fileData[20].numFile; i++) {
        cityHallImage[i].addEventListener("mouseover", overCityHallItem, false);
        cityHallImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        cityHallImage[i].addEventListener("click", clickCityHallItem, false);
    }
    addIconListeners();
}

function removeCityHallListeners() {
    for (var i = 6; i < fileData[20].numFile; i++) {
        cityHallImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}