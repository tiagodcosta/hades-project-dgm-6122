function office() {
    sceneLocation = "office";
    currentBackground = officeImage[1];
    updateMap(mapImage[2]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(officeImage[1]);
    
    if (firstEmailRead == true) {
        scene.addChild(officeImage[8]);
        scene.addChild(officeImage[9]); 
    } else {
        scene.addChild(officeImage[7]);
    }
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(officeImage[0], -1100, -685);
}

function talkCoworkerA() {
    removeListeners();
    officeImage[7].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(officeImage[2]);
    officeImage[2].alpha = 0;
    
    resetSubtitle(3);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(officeImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(officeImage[7], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (coworkerATalked == true) {
                                  officeImage[1].addEventListener("click", function() {back(false);}, false);
                              } else {
                                  coworkerATalked = true;
                              }
                              scene.removeChild(officeImage[7]);
                              playDialogue("CoworkerA");
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {back(true);});});
    
    function back(done) {
        if (done == true && emailMode == 0) {
            setTimeout(function() {newEmail(1);}, emailDelay);
        }
        
        officeImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(officeImage[7]);
        officeImage[7].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(officeImage[7], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(officeImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(officeImage[2]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function talkCoworkerB() {
    removeListeners();
    officeImage[8].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    var noteHanded = false;
    
    scene.addChild(officeImage[3]);
    officeImage[3].alpha = 0;
    
    resetSubtitle(4);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(officeImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(officeImage[8], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (coworkerBTalked == true) {
                                  officeImage[1].addEventListener("click", back, false);
                              } else {
                                  coworkerBTalked = true;
                              }
                              scene.removeChild(officeImage[8]);
                              playDialogue("CoworkerB");
                              scene.addChild(subtitle);
                              if (patientAccess == true) {
                                  changeSubtitle(0, back);
                              } else {
                                  changeSubtitle(0, handNote);
                              }});
    
    function handNote() {
        noteHanded = true;
        
        scene.addChild(officeImage[5]);
        scene.addChild(officeImage[10]);
        officeImage[5].alpha = 0;
        officeImage[10].alpha = 0;
        
        scene.removeChild(subtitleBox);
        
        createjs.Tween.get(officeImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(officeImage[10], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(officeImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance = createjs.Sound.play("HandObject");
                              scene.removeChild(officeImage[3]);
                              rolloverBox.alpha = 0.8;
                              officeImage[10].addEventListener("mouseover", overOfficeItem, false);
                              officeImage[10].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
                              officeImage[10].addEventListener("click", clickOfficeItem, false);
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
    
    function back() {
        officeImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(officeImage[8]);
        officeImage[8].alpha = 0;        
        
        if(noteHanded == true) {
            var num = 5;
            scene.removeChild(officeImage[10]);
        } else {
            var num = 3;
        }
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(officeImage[8], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(officeImage[num], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(officeImage[num]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function talkBoss() {
    removeListeners();
    officeImage[9].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(officeImage[4]);
    officeImage[4].alpha = 0;
    
    resetSubtitle(5);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(officeImage[4], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(officeImage[9], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (bossTalked == true) {
                                  officeImage[1].addEventListener("click", function() {back(false);}, false);
                              } else {
                                  bossTalked = true;
                              }
                              scene.removeChild(officeImage[9]);
                              playDialogue("Boss");
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {back(true);});});
    
    function back(done) {
        if (done == true && noteAccess[1] == false) {
            addNote(1);
        }
        
        officeImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(officeImage[9]);
        officeImage[9].alpha = 0;

        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(officeImage[9], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(officeImage[4], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(officeImage[4]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function viewNote() {
    soundInstance = createjs.Sound.play("ViewObject", loopSound);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    officeImage[1].removeAllEventListeners();
    officeImage[1].addEventListener("click", back, false);
    
    scene.addChild(officeImage[8]);
    officeImage[8].alpha = 0;
    
    scene.addChild(greyLayer);
    greyLayer.alpha = 0;
    
    scene.addChild(officeImage[6]);
    officeImage[6].x = 530;
    officeImage[6].y = 430;
    officeImage[6].scaleX = 1/10;
    officeImage[6].scaleY = 1/10;
    
    scene.removeChild(officeImage[10]);

    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(officeImage[6], {loop: false}).to({x:300, y:150, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(officeImage[8], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0.7}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(officeImage[5], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
        .call(function() {scene.removeChild(officeImage[5]);
                          createjs.Ticker.removeEventListener("tick", stage);});
    
    function back() {
        officeImage[1].removeAllEventListeners();
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(officeImage[6], {loop: false}).to({x:160, y:50, scaleX:1/10, scaleY:1/10}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance.stop();
                              scene.removeChild(greyLayer);
                              scene.removeChild(officeImage[6]);
                              patientAccess = true;
                              newLocation(mapImage[3]);
                              newLocation(mapImage[4]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overOfficeItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 7:
            rollover.text = "Talk to coworker";
            break;
        case 8:
            rollover.text = "Tell coworker about email";
            break; 
        case 9:
            rollover.text = "Tell boss about email";
            break;
        case 10:
            rollover.text = "Take note";
            break;
    }
    addShadow(event, "#222222");
}

function clickOfficeItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 7:
            talkCoworkerA();
            break; 
        case 8:
            talkCoworkerB();
            break; 
        case 9:
            talkBoss();
            break; 
        case 10:
            viewNote();
    }
}

function addOfficeListeners() {
    for (var i = 7; i < fileData[15].numFile; i++) {
        officeImage[i].addEventListener("mouseover", overOfficeItem, false);
        officeImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        officeImage[i].addEventListener("click", clickOfficeItem, false);
    }
    addIconListeners();
}

function removeOfficeListeners() {
    for (var i = 7; i < fileData[15].numFile; i++) {
        officeImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}