function setupNotes() {
    noteNum = 0;
    
    notes = new createjs.Container();
    
    notes.addChild(notesImage[0]);
    
    notesImage[0].addEventListener("click", function() {}, false);
    
    for (var i = 0; i < 11; i++) {
        noteAccess[i] = false;
    }
}

function addNote(num) {
    noteAccess[num] = true;
    
    if (noteNum < 5) {
        notesImage[num].x = 110;
    } else {
        notesImage[num].x = 530;
    }
    notesImage[num].y = 125 + (noteNum % 5)*90;
    
    notes.addChild(notesImage[num]);
    noteNum++;
}