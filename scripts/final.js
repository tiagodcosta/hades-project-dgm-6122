function finalOptions() {
    soundInstanceBG = createjs.Sound.play("FinalOptions", {volume:0.8, loop:-1});
    
    email.addChild(emailImage[1]);
    email.addChild(tabletImage[15]);
    
    for (var i = 41; i < 44; i++) {
        emailImage[i].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
        emailImage[i].addEventListener("mouseout", removeShadow, false);
        emailImage[i].addEventListener("click", optionSelected, false);

        email.addChild(emailImage[i]);
    }
    stage.update();
}

function optionSelected(event) {
    finalReply = true;
    
    var id = event.currentTarget.id;
    
    switch(id) {
        case 41:
            option = 0;
            break;
        case 42:
            option = 1;
            break;
        case 43:
            option = 2;
            break;
    }
    
    emailImage[40].removeAllEventListeners();
    email.removeChild(emailImage[40]);
    email.removeChild(emailImage[1]);
    email.removeChild(emailImage[41]);
    email.removeChild(emailImage[42]);
    email.removeChild(emailImage[43]);
    
    soundInstanceBG.stop();
    soundInstanceBG = createjs.Sound.play("EndScene", {volume:0.3, loop:-1});
    
    slideDown();
    removeListeners();
    
    map.removeAllChildren();
    map.addChild(mapImage[0]);
    map.addChild(mapImage[1]);
    
    setTimeout(home, 1000);
    setTimeout(function() {newEmail(4);}, 3500+emailDelay);
}

function showNewspaper() {
    final = new createjs.Container();
    final.alpha = 0;
    stage.addChild(final);
    
    final.addChild(finalImage[option+4]);
    
    for (var i = 4; i < fileData[28].numFile; i++) {
        finalImage[i].addEventListener("mouseover", overFinalItem, false);
        finalImage[i].addEventListener("mouseout", removeShadow, false);
        finalImage[i].addEventListener("click", clickFinalItem, false);
    }
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(scene, {loop: false}).to({alpha:0}, 1000, createjs.Ease.quadInOut);
    createjs.Tween.get(final, {loop: false}).wait(1000).to({alpha:1}, 1000, createjs.Ease.quadInOut)
        .call(function() {stage.removeChild(scene);
                          createjs.Ticker.removeEventListener("tick", stage);});
}

function credits() {
    final.addChild(finalImage[2]);
    final.addChild(finalImage[7]);
    
    createjs.Tween.get(final, {loop: false}).to({alpha:1}, 1000, createjs.Ease.quadInOut)
        .call(function() {createjs.Ticker.removeEventListener("tick", stage);});
}

function voiceActors() {
    final.addChild(finalImage[3]);
    final.addChild(finalImage[8]);
    final.addChild(finalImage[9]);
    
    createjs.Tween.get(final, {loop: false}).to({alpha:1}, 1000, createjs.Ease.quadInOut)
        .call(function() {createjs.Ticker.removeEventListener("tick", stage);});
}

function overFinalItem(event) {
    var id = event.currentTarget.id;
    
    switch(id) {
        case 4:
        case 5:
        case 6:
            addShadow(event, "#777777");
            break;
        case 7:
        case 8:
        case 9:
            addShadow(event, "#f55b29");
            break;
    }
}
function clickFinalItem(event) {
    var id = event.currentTarget.id;
    
    switch(id) {
        case 4:
        case 5:
        case 6:
            soundInstanceBG.stop();
            soundInstanceBG = createjs.Sound.play("Credits", {volume:0.5, loop:-1});
            
            createjs.Ticker.addEventListener("tick", stage); 
            createjs.Tween.get(final, {loop: false}).to({alpha:0}, 1000, createjs.Ease.quadInOut)
                .call(function() {final.removeChild(finalImage[option+4]);
                                  credits();});
            break;
        case 7:
            createjs.Ticker.addEventListener("tick", stage); 
            createjs.Tween.get(final, {loop: false}).to({alpha:0}, 1000, createjs.Ease.quadInOut)
                .call(function() {final.removeChild(finalImage[2]);
                                  final.removeChild(finalImage[7]);
                                  voiceActors();});
            break;
        case 8:
            createjs.Ticker.addEventListener("tick", stage); 
            createjs.Tween.get(final, {loop: false}).to({alpha:0}, 1000, createjs.Ease.quadInOut)
                .call(function() {final.removeChild(finalImage[3]);
                                  final.removeChild(finalImage[8]);
                                  final.removeChild(finalImage[9]);
                                  credits();});
            break;
        case 9:
            createjs.Ticker.addEventListener("tick", stage); 
            createjs.Tween.get(final, {loop: false}).to({alpha:0}, 1000, createjs.Ease.quadInOut)
                .call(function() {soundInstanceBG.stop();
                                  stage.removeChild(final);
                                  resetIF();});
            break;
    }
}