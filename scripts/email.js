function setupEmail() {
    firstEmailRead = secondEmailRead = finalEmailRead = false;
    nemoCount = 0;
    finalReply = false;
    
    email = new createjs.Container();
    
    email.addChild(emailImage[0]);
    
    emailImage[0].addEventListener("click", function() {}, false);
    
    for (var i = 35; i < 40; i++) {
        emailImage[i].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
        emailImage[i].addEventListener("mouseout", removeShadow, false);
        emailImage[i].addEventListener("click", clickEmail, false);

        email.addChild(emailImage[i]);
    }
    
    emailTabs = new createjs.Container();
    emailTabs.x = 58;
    emailTabs.y = 60;
    
    // 0 - Initial emails 2, 1 - First Nemo email, 2 - Second Nemo email, 3 - Third Nemo email, 4 - Final Nemo email
    emailMode = 0;
    setEmailTabs();
    
    email.addChild(emailTabs);
    
    currentEmail = 4;
    updateEmail(4);  
}

function setEmailTabs() {
    emailTabs.removeAllChildren();
    
    for (var i = emailMode+2; i < emailMode+6; i++) {
        emailTabs.addChild(emailImage[i]);
        emailImage[i].y = 116 * (6 - (i - emailMode));
    }
    
    if (emailMode == 4) {
        i = emailMode + option + 6;
    } else {
        i = emailMode + 6;
    }
    emailTabs.addChild(emailImage[i]);
    emailImage[i].y = 0;
}

function updateEmail(num) {
    var prevEmail = currentEmail;
    
    emailImage[35+prevEmail].filters = [filterNone];
    emailImage[35+prevEmail].cache(0,0,1000,700);
    if (emailMode == 4 & prevEmail == 4) {
        email.removeChild(emailImage[13 + prevEmail + emailMode + option]);
        email.removeChild(emailImage[24 + prevEmail + emailMode + option]); 
    } else {
        email.removeChild(emailImage[13 + prevEmail + emailMode]);
        email.removeChild(emailImage[24 + prevEmail + emailMode]);   
    }
    
    currentEmail = num;
    
    emailImage[35+currentEmail].filters = [filterEmailCurrent];
    emailImage[35+currentEmail].cache(0,0,1000,700);
    if (emailMode == 4 && currentEmail == 4) {
        email.addChild(emailImage[13 + currentEmail + emailMode + option]);
        email.addChild(emailImage[24 + currentEmail + emailMode + option]); 
    } else {
        email.addChild(emailImage[13 + currentEmail + emailMode]);
        email.addChild(emailImage[24 + currentEmail + emailMode]); 
    }
    
    if (firstEmailRead == false && emailMode == 1 && currentEmail == 4) {
        firstEmailRead = true;
        if (sceneLocation == "office") {
            scene.removeChild(officeImage[7]);
            scene.addChild(officeImage[8]);
            scene.addChild(officeImage[9]); 
        }
    }
    
    if (secondEmailRead == false && emailMode == 2 && currentEmail == 4) {
        secondEmailRead = true;
        newLocation(mapImage[11]);
    }
    
    if (finalReply == false && emailMode == 3 && currentEmail == 4) {
        emailImage[40].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
        emailImage[40].addEventListener("mouseout", removeShadow, false);
        emailImage[40].addEventListener("click", finalOptions, false);
        
        email.addChild(emailImage[40]);
    } else {
        emailImage[40].removeAllEventListeners();
        email.removeChild(emailImage[40]);
    }
    
    if (finalEmailRead == false && emailMode == 4 && currentEmail == 4) {
        finalEmailRead = true;
    }
    
    stage.update();
}

function newEmail(num) {
    soundInstance = createjs.Sound.play("EmailNotification");
    
    email.removeChild(emailImage[13 + emailMode]);
    email.removeChild(emailImage[24 + emailMode]);
    
    emailMode = num;
    setEmailTabs();
    
    if (currentEmail != 0) {
        var view = currentEmail - 1;
    } else {
        view = 0;
    }
        
    updateEmail(view);
    
    emailImage[39].filters = [filterEmailNew];
    emailImage[39].cache(0,0,1000,700);
    
    tabletNotification = true;
    scene.addChild(iconImage[0]);
    
    emailNotification = true;
    tablet.addChild(tabletImage[2]);
    
    stage.update();
}

function clickEmail(event) {
    var id = event.currentTarget.id;

    soundInstance = createjs.Sound.play("ClickApp");
    
    updateEmail(id-35);
}