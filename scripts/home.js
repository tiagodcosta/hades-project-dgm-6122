function home() {
    sceneLocation = "home";
    
    if (finalReply == false) {
        homeExterior = homeImage[0];
        homeInterior = homeImage[1];  
    } else {
        homeExterior = finalImage[0];
        homeInterior = finalImage[1];
    }
    homeInterior.removeAllEventListeners();
    
    currentBackground = homeInterior;
    updateMap(mapImage[1]);  
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(homeInterior);
    scene.addChild(homeImage[3]);
    scene.addChild(homeImage[4]);
    scene.addChild(homeImage[5]);
    
    if (tabletAccess == true) {
        scene.addChild(iconImage[2]);
        scene.addChild(iconImage[3]);
    } else {
        scene.addChild(homeImage[2]);
        homeImage[2].x = 100;
        homeImage[2].y = 455;
    }
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(homeExterior, -1855, -1450);
}

function pickTablet() {
    tabletAccess = true;
    rollover.text = "";
    
    soundInstance = createjs.Sound.play("PickupTablet");
    
    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(homeImage[2], {loop: false}).to({x:50, y:15}, 400, createjs.Ease.quadInOut)
            .call(function () {scene.removeChild(homeImage[2]);
                               scene.addChild(iconImage[2]);
                               scene.addChild(iconImage[3]);
                               createjs.Ticker.removeEventListener("tick", stage);});
}

function momFlashback() {
    soundInstanceBG = createjs.Sound.play("Flashback", {volume:0.3});
    
    removeListeners();
    flashback.removeAllChildren();
    homeImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;

    stage.addChild(flashback);
    flashback.alpha = 1;
    flashback.x = 670;
    flashback.y = 230;
    flashback.scaleX = 1/10;
    flashback.scaleY = 1/10;
    
    flashback.addChild(momFlashbackImage[0]);
    
    flashback.addChild(momFlashbackImage[1]);
    momFlashbackImage[1].alpha = 0;
    momFlashbackImage[1].x = 0;
    momFlashbackImage[1].y = 0;
    momFlashbackImage[1].scaleX = 1/4;
    momFlashbackImage[1].scaleY = 1/4;
    
    flashback.addChild(momFlashbackImage[2]);
    momFlashbackImage[2].alpha = 0;
    
    resetSubtitle(0);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(flashback, {loop: false}).to({x:0, y:0, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(momFlashbackImage[1], {loop: false}).wait(500).to({alpha:1}, 1500, createjs.Ease.quadInOut);
    createjs.Tween.get(momFlashbackImage[2], {loop: false}).wait(500).to({alpha:1}, 1500, createjs.Ease.quadInOut)
            .call(playFlashback);
    
    function playFlashback() {
        if (momFlashWatched == true) {
            homeInterior.addEventListener("click", back, false);
        } else {
            momFlashWatched = true;
        }
        momFlashbackImage[0].alpha = 0;
        
        flashback.addChild(subtitleBox);
        flashback.addChild(subtitle);
        changeSubtitle(0, function() {flashback.removeChild(subtitle); flashback.removeChild(subtitleBox);});
        soundInstance = createjs.Sound.play("MomFlashback");
        
        createjs.Tween.get(momFlashbackImage[1], {loop: false}).to({x:-1750, y:-725, scaleX:0.8, scaleY:0.8}, 19000, createjs.Ease.quadInOut).to({x:-865, y:-755, scaleX:0.7, scaleY:0.7}, 13000, createjs.Ease.quadInOut).to({x:0, y:0, scaleX:1/4, scaleY:1/4}, 2000, createjs.Ease.quadInOut)
            .call(back);
    }
    
    function back() {
        homeInterior.removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        
        momFlashbackImage[0].alpha = 1;
        
        createjs.Tween.get(momFlashbackImage[1], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(momFlashbackImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(flashback, {loop: false}).wait(500).to({x:670, y:230, scaleX:1/10, scaleY:1/10}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstanceBG.stop();
                              stage.removeChild(flashback);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function uncleFlashback() {
    soundInstanceBG = createjs.Sound.play("Flashback", {volume:0.3});
    
    removeListeners();
    flashback.removeAllChildren();
    homeImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    stage.addChild(flashback);
    flashback.alpha = 1;
    flashback.x = 865;
    flashback.y = 230;
    flashback.scaleX = 1/10;
    flashback.scaleY = 1/10;
    
    flashback.addChild(uncleFlashbackImage[0]);
    
    flashback.addChild(uncleFlashbackImage[1]);
    uncleFlashbackImage[1].alpha = 0;
    uncleFlashbackImage[1].x = 0;
    uncleFlashbackImage[1].y = 0;
    uncleFlashbackImage[1].scaleX = 1/4;
    uncleFlashbackImage[1].scaleY = 1/4;
    
    flashback.addChild(uncleFlashbackImage[2]);
    uncleFlashbackImage[2].alpha = 0;
    
    resetSubtitle(1);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(flashback, {loop: false}).to({x:0, y:0, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(uncleFlashbackImage[1], {loop: false}).wait(500).to({alpha:1}, 1500, createjs.Ease.quadInOut);
    createjs.Tween.get(uncleFlashbackImage[2], {loop: false}).wait(500).to({alpha:1}, 1500, createjs.Ease.quadInOut)
            .call(playFlashback);
    
    function playFlashback() {
        homeInterior.addEventListener("click", back, false);
        uncleFlashbackImage[0].alpha = 0;
        
        flashback.addChild(subtitleBox);
        flashback.addChild(subtitle);
        changeSubtitle(0, function() {flashback.removeChild(subtitle); flashback.removeChild(subtitleBox);});
        soundInstance = createjs.Sound.play("UncleFlashback");
        
        createjs.Tween.get(uncleFlashbackImage[1], {loop: false}).to({x:-435, y:-785, scaleX:0.7, scaleY:0.7}, 5000, createjs.Ease.quadInOut).to({x:-1790, y:-1080}, 6000, createjs.Ease.quadInOut).to({x:0, y:0, scaleX:1/4, scaleY:1/4}, 4000, createjs.Ease.quadInOut)
            .call(back);
    }
    
    function back() {
        homeInterior.removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        
        uncleFlashbackImage[0].alpha = 1;
        
        createjs.Tween.get(uncleFlashbackImage[1], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(uncleFlashbackImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(flashback, {loop: false}).wait(500).to({x:865, y:230, scaleX:1/10, scaleY:1/10}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstanceBG.stop();
                              stage.removeChild(flashback);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function sisterFlashback() {
    soundInstanceBG = createjs.Sound.play("Flashback", {volume:0.3});
    
    removeListeners();
    flashback.removeAllChildren();
    homeImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    stage.addChild(flashback);
    flashback.alpha = 1;
    flashback.x = 685;
    flashback.y = 145;
    flashback.scaleX = 1/10;
    flashback.scaleY = 1/10;
    
    flashback.addChild(sisterFlashbackImage[0]);
    
    flashback.addChild(sisterFlashbackImage[1]);
    sisterFlashbackImage[1].alpha = 0;
    sisterFlashbackImage[1].x = 0;
    sisterFlashbackImage[1].y = 0;
    sisterFlashbackImage[1].scaleX = 1/4;
    sisterFlashbackImage[1].scaleY = 1/4;
    
    flashback.addChild(sisterFlashbackImage[2]);
    sisterFlashbackImage[2].alpha = 0;
    
    resetSubtitle(2);

    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(flashback, {loop: false}).to({x:0, y:0, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(sisterFlashbackImage[1], {loop: false}).wait(500).to({alpha:1}, 1500, createjs.Ease.quadInOut);
    createjs.Tween.get(sisterFlashbackImage[2], {loop: false}).wait(500).to({alpha:1}, 1500, createjs.Ease.quadInOut)
            .call(playFlashback);
    
    function playFlashback() {
        if (sisFlashWatched == true) {
            homeInterior.addEventListener("click", back, false);
        } else {
            sisFlashWatched = true;
        }
        sisterFlashbackImage[0].alpha = 0;
        
        flashback.addChild(subtitleBox);
        flashback.addChild(subtitle);
        changeSubtitle(0, function() {flashback.removeChild(subtitle); flashback.removeChild(subtitleBox);});
        soundInstance = createjs.Sound.play("SisterFlashback");
        
        createjs.Tween.get(sisterFlashbackImage[1], {loop: false}).to({x:-1030, y:-780, scaleX:0.8, scaleY:0.8}, 3000, createjs.Ease.quadInOut).to({x:-1220, y:-1045}, 3000, createjs.Ease.quadInOut).to({x:-750, y:-590, scaleX:0.6, scaleY:0.6}, 7000, createjs.Ease.quadInOut).to({x:-1345, y:-2100, scaleX:1, scaleY:1}, 5000, createjs.Ease.quadInOut).to({x:0, y:0, scaleX:1/4, scaleY:1/4}, 2000, createjs.Ease.quadInOut)
            .call(back);
    }
    
    function back() {
        homeInterior.removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        
        sisterFlashbackImage[0].alpha = 1;
    
        createjs.Tween.get(sisterFlashbackImage[1], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(sisterFlashbackImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(flashback, {loop: false}).wait(500).to({x:685, y:145, scaleX:1/10, scaleY:1/10}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstanceBG.stop();
                              stage.removeChild(flashback);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overHomeItem(event) {
    var id = event.currentTarget.id;
    switch(id) {
        case 2:
            rollover.text = "Pick up tablet";
            break;
        case 3:
            rollover.text = "Look at photo with Mom";
            break;
        case 4:
            rollover.text = "Look at photo with Uncle Aaron";
            break;
        case 5:
            rollover.text = "Look at photo with Courtney";
            break;
    }
    addShadow(event, "#222222");
}

function clickHomeItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 2:
            pickTablet();
            break;
        case 3:
            momFlashback();
            break;
        case 4:
            uncleFlashback();
            break;
        case 5:
            sisterFlashback();
            break;
    }
}

function addHomeListeners() {
    for(var i = 2; i < fileData[11].numFile; i++) {
        homeImage[i].addEventListener("mouseover", overHomeItem, false);
        homeImage[i].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
        homeImage[i].addEventListener("click", clickHomeItem, false);
    }
    addIconListeners();
}

function removeHomeListeners() {
    for(var i = 2; i < fileData[11].numFile; i++) {
        homeImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}