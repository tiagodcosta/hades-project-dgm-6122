function setupMedical() {
    medical = new createjs.Container();
    
    medical.addChild(medicalImage[0]);
    medical.addChild(medicalImage[1]);
    medical.addChild(medicalImage[2]);
    
    medicalImage[0].addEventListener("click", function() {}, false);
    
    medicalImage[2].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
    medicalImage[2].addEventListener("mouseout", removeShadow, false);
    medicalImage[2].addEventListener("click", showMedicalGraphs, false);
}

function showMedicalGraphs() {
    medical.addChild(medicalImage[3]);
    
    medical.removeChild(medicalImage[1]);
    medical.removeChild(medicalImage[2]);
    
    stage.update();
    
    addNote(2);
}