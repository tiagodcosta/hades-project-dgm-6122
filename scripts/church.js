function church() {
    sceneLocation = "church";
    currentBackground = churchImage[1];
    updateMap(mapImage[9]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(churchImage[1]);
    scene.addChild(churchImage[3]);

    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(churchImage[0], -950, -1680);
}

function talkClergy() {
    removeListeners();
    churchImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(churchImage[2]);
    churchImage[2].alpha = 0;
    
    resetSubtitle(16);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(churchImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(churchImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (clergyTalked == true) {
                                  churchImage[1].addEventListener("click", function() {back(false);}, false);
                              } else {
                                  clergyTalked = true;
                              }
                              scene.removeChild(churchImage[3]);
                              playDialogue("Clergy");
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {back(true);});});
    
    function back(done) {
        if (done == true && noteAccess[7] == false) {
            addNote(7);    
        }
        
        churchImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(churchImage[3]);
        churchImage[3].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(churchImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(churchImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(churchImage[2]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overChurchItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            rollover.text = "Talk to clergyman";
            break; 
    }
    addShadow(event, "#222222");
}

function clickChurchItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            talkClergy();
            break; 
    }
}

function addChurchListeners() {
    for (var i = 3; i < fileData[22].numFile; i++) {
        churchImage[i].addEventListener("mouseover", overChurchItem, false);
        churchImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        churchImage[i].addEventListener("click", clickChurchItem, false);
    }
    addIconListeners();
}

function removeChurchListeners() {
    for (var i = 3; i < fileData[22].numFile; i++) {
        churchImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}