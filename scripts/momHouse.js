function momHouse() {
    sceneLocation = "momHouse";
    currentBackground = momImage[1];
    updateMap(mapImage[13]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(momImage[1]);
    scene.addChild(momImage[3]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(momImage[0], -1265, -1550);
}

function talkMom() {
    removeListeners();
    momImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(momImage[2]);
    warehouseImage[2].alpha = 0;
    
    resetSubtitle(23);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(momImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(momImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (momTalked == true) {
                                  momImage[1].addEventListener("click", function() {back(false);}, false);
                              } else {
                                  momTalked = true;
                              }
                              scene.removeChild(momImage[6]);
                              playDialogue("Mom");
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {back(true);});});
    
    function back(done) {
        soundInstance.stop();
        if (done == true && rehabAccess == false) {
            rehabAccess = true;
            newLocation(mapImage[14]);
        }

        momImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(momImage[3]);
        momImage[3].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(momImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(momImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(momImage[2]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overMomHouseItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            rollover.text = "Ask mom about Uncle Aaron";
            break; 
    }
    addShadow(event, "#222222");
}

function clickMomHouseItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            talkMom();
            break; 
    }
}

function addMomHouseListeners() {
    for (var i = 3; i < fileData[26].numFile; i++) {
        momImage[i].addEventListener("mouseover", overMomHouseItem, false);
        momImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        momImage[i].addEventListener("click", clickMomHouseItem, false);
    }
    addIconListeners();
}

function removeMomHouseListeners() {
    for (var i = 3; i < fileData[26].numFile; i++) {
        momImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}