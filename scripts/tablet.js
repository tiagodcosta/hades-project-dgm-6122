function setupTablet() {
    mapNotification = tabletNotification = emailNotification = false;
    appNotification = [false, false, false, false]; // [medical, biodata, criminal, research]
    tabletAccess = medicalAccess = biodataAccess = criminalAccess = researchAccess = false;
    apps = 0;
    
    tabletImage[0].addEventListener("click", function() {}, false);
    
    var i;
    for (i = 7; i < fileData[2].numFile; i++) {
        tabletImage[i].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
        tabletImage[i].addEventListener("mouseout", removeShadow, false);
        tabletImage[i].addEventListener("click", clickTablet, false);
    }
    
    tablet = new createjs.Container();
    tablet.addChild(tabletImage[0]);
    tablet.addChild(tabletImage[16]);
    
    tabletImage[1].x = 240;
    tabletImage[1].y = 155;
    tabletImage[2].x = 435;
    tabletImage[2].y = 155;
    
    for (i = 7; i < 11; i++) {
        tablet.addChild(tabletImage[i]);
        tabletImage[i].y = 160;
        tabletImage[i].x = 145 + apps*195;
        apps++;
    }
    
    setupMap();
    setupEmail();
    setupBrowser();
    setupNotes();
    setupMedical();
    setupBiodata();
    setupCriminal();
    setupResearch();
}

function newApp(num) {
    soundInstance = createjs.Sound.play("AppUnlocked");
    
    var image = tabletImage[11+num];
    tablet.addChild(image);
    image.y = 380;
    image.x = 145 + (apps-4)*195;
    
    appNotification[num] = true;
    tablet.addChild(tabletImage[3+num]);
    tabletImage[3+num].x = 240 + (apps-4)*195; 
    tabletImage[3+num].y = 375;
        
    apps++;
    
    tabletNotification = true;
    scene.addChild(iconImage[0]);
    iconImage[0].alpha = 0.5;
}

function slideUp() {
    removeListeners();
    rollover.text = "";
    currentBackground.addEventListener("click", function() {slideDown();}, false);
    
    if (currentApp == tablet) {
        tablet.addChild(tabletImage[16]);
        iconImage[2].shadow = new createjs.Shadow("#000000", 0, 0, 0);
        
        if (tabletNotification == true) {
            tabletNotification = false;
            scene.removeChild(iconImage[0]);
        }
    } else {
        map.addChild(tabletImage[15]);
        map.addChild(tabletImage[16]);
        iconImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
        
        if (mapNotification == true) {
            mapNotification = false;
            scene.removeChild(iconImage[1]);
            tablet.removeChild(tabletImage[1]);
        }
    }
    
    stage.addChild(currentApp);
    currentApp.y = 700;
    
    soundInstance = createjs.Sound.play("SlideTablet");
    
    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(currentApp, {loop: false}).to({y:0}, 700, createjs.Ease.quadInOut)
            .call(function () {soundInstance.stop();
                               createjs.Ticker.removeEventListener("tick", stage);});
}

function slideDown() {
    currentBackground.removeAllEventListeners();
    
    soundInstance = createjs.Sound.play("SlideTablet");
    
    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(currentApp, {loop: false}).to({y:700}, 700, createjs.Ease.quadInOut)
            .call(function () {soundInstance.stop();
                               addListeners();
                               stage.removeChild(currentApp);
                               createjs.Ticker.removeEventListener("tick", stage);
                               if (nemoCount == 2 && emailMode < 2) {
                                   setTimeout(newEmail(2), emailDelay);
                               }
                               if (finalEmailRead == true) {
                                   removeListeners();
                                   setTimeout(showNewspaper, 1000);
                               }});
}

function showApp(app) {
    if (app == tablet) {
        if (tabletNotification == true) {
            tabletNotification = false;
            scene.removeChild(iconImage[0]);
        }
    } else {
        app.addChild(tabletImage[15]);    
    }
    
    stage.addChild(app);
    app.y = 0;
    app.alpha = 0;
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(app, {loop: false}).to({alpha:1}, 300, createjs.Ease.quadInOut)
            .call(function() {app.addChild(tabletImage[16]);
                              stage.removeChild(currentApp);
                              currentApp = app;
                              createjs.Ticker.removeEventListener("tick", stage);});   
}

function clickTablet(event) {
    var id = event.currentTarget.id;
    
    if (id != 16) {
        soundInstance = createjs.Sound.play("ClickTablet");
    }
    
    switch(id) {
        case 7:
            if (mapNotification == true) {
                mapNotification = false;
                scene.removeChild(iconImage[1]);
                tablet.removeChild(tabletImage[1]);
            }
            setTimeout(function() {showApp(map);}, tabletClickDelay);
            break;
        case 8:
            if (emailNotification == true) {
                emailNotification = false;
                tablet.removeChild(tabletImage[2]);
            }
            setTimeout(function() {showApp(email);}, tabletClickDelay);
            break;
        case 9:
            setTimeout(function() {showApp(browser);}, tabletClickDelay);
            break;
        case 10:
            setTimeout(function() {showApp(notes);}, tabletClickDelay);
            break;
        case 11:
            if (appNotification[0] == true) {
                appNotification[0] = false;
                tablet.removeChild(tabletImage[3]);
            }
            setTimeout(function() {showApp(medical);}, tabletClickDelay);
            break;
        case 12:
            if (appNotification[1] == true) {
                appNotification[1] = false;
                tablet.removeChild(tabletImage[4]);
            }
            setTimeout(function() {showApp(biodata);}, tabletClickDelay);
            break;
        case 13:
            if (appNotification[2] == true) {
                appNotification[2] = false;
                tablet.removeChild(tabletImage[5]);
            }
            setTimeout(function() {showApp(criminal);}, tabletClickDelay);
            break;
        case 14:
            if (appNotification[3] == true) {
                appNotification[3] = false;
                tablet.removeChild(tabletImage[6]);
            }
            if (noteAccess[3] == false) {
                addNote(3);
                nemoCount++;
            }
            setTimeout(function() {showApp(research);}, tabletClickDelay);
            break;
        case 15:
            setTimeout(function() {showApp(tablet);}, tabletClickDelay);
            break;
        case 16:
            slideDown();
            break;
    }
}