// RESOURCES
// EaselJS: http://www.createjs.com/easeljs
// TweenJS: http://www.createjs.com/tweenjs
// SoundJS: http://www.createjs.com/soundjs
// PreloadJS: http://www.createjs.com/preloadjs
// Brackets 1.5: http://brackets.io
// BitBucket: https://bitbucket.org
// Sublime Text: https://www.sublimetext.com/
// CodeKit: https://incident57.com/codekit/

// HTML5 CANVAS

// SETUP
// Image arrays
var titleImage = [], tabletImage = [], iconImage = [];
var mapImage = [], emailImage = [], browserImage = [], notesImage = [];
var medicalImage = [], biodataImage = [], criminalImage = [], researchImage = [];
var homeImage = [], momFlashbackImage = [], uncleFlashbackImage = [], sisterFlashbackImage = [];
var officeImage = [], patientAImage = [], patientBImage = [], hospitalImage = [];
var libraryImage = [], cityHallImage = [], policeImage = [], churchImage = [], prisonImage = [];
var warehouseImage = [], auntImage = [], momImage = [], rehabImage = [], finalImage = [];
// Containers
var tablet, map, email, emailTabs;
var browser, notes, medical, biodata, criminal, research;
var scene, flashback, final;

var subtitle, subtitleText, subtitleDelay, subtitleDuration, numSubtitle;
var subtitleBox, rolloverBox, rollover, soundInstance, soundInstanceBG, greyLayer;
var sceneLocation, currentBackground, currentMapImage;
var homeInterior, homeExterior;

var apps, currentApp, currentEmail;
var browserPage, noteNum, noteAccess = [];

var momFlashWatched, sisFlashWatched;
var coworkerATalked, coworkerBTalked, bossTalked;
var patientTalked, familyATalked, familyBTalked;
var doctorTalked, nurseTalked, hospAdminTalked;
var researcherTalked, mayorTalked, cityAdminTalked;
var policeTalked, clergyTalked, prisonerATalked, prisonerBTalked;
var nemoTalked, auntTalked, momTalked, courtneyTalked;

var mapNotification, tabletNotification, emailNotification, appNotification = [];
var patientAccess, hospitalAccess, libraryAccess, cityHallAccess;
var policeAccess, churchAccess, prisonAccess;
var familyAccess, rehabAccess;
var tabletAccess, medicalAccess, biodataAccess, criminalAccess, researchAccess;
var firstEmailRead, secondEmailRead, finalEmailRead;
var nemoCount, finalReply, option;

var tabletClickDelay = 300, emailDelay = 1000;
var iconX = [114, 204, 30, 150], iconY = [26, 26, 30, 30];

var filterNone = new createjs.ColorFilter();
var filterMapNew = new createjs.ColorFilter(1,1,1,1,80,0,0,0);
var filterMapCurrent = new createjs.ColorFilter(1,1,1,1,0,50,0,0);
var filterEmailNew = new createjs.ColorFilter(1,1,1,1,40,0,0,0);
var filterEmailCurrent = new createjs.ColorFilter(1,1,1,1,0,20,0,0);

var loopSound = new createjs.PlayPropsConfig().set({loop: -1});

// Initialize canvas
var canvas = document.getElementById("canvas");
var stage = new createjs.Stage("canvas");
createjs.Ticker.setFPS(30);
stage.enableMouseOver();  

// Play loading sound
createjs.Sound.on("fileload", function() {soundInstance = createjs.Sound.play("LoadingScreen");});
createjs.Sound.registerSound("sounds/effects/loadScreen.m4a", "LoadingScreen");

// Show loading progress
var loadText = new createjs.Text("0% Loaded", "40px Monda", "#3d251d");
loadText.x = 500;
loadText.y = 250;
loadText.textAlign = "center";
stage.addChild(loadText);
var loadBar = new createjs.Shape();
loadBar.graphics.beginFill("#3d251d");
loadBar.graphics.drawRect(0, 0, 1, 30);
loadBar.x = 250;
loadBar.y = 350;
stage.addChild(loadBar);
stage.update();

// Preload image and video files
var fileQueue = new createjs.LoadQueue(false);
fileQueue.on("progress", updateProgress);
fileQueue.on("complete", processData);
fileQueue.loadManifest(fileList);
fileQueue.next = soundQueue;
// Preload sound files
var soundQueue = new createjs.LoadQueue(false);
soundQueue.installPlugin(createjs.Sound);
soundQueue.loadManifest(soundList);


// FUNCTIONS
// Update loading progress
function updateProgress(event) {
    var progress = Math.round(event.progress * 100);
    loadText.text = progress + "% Loaded";
    loadBar.graphics.drawRect(0, 0, progress*5, 30);
    stage.update();
}
    
// Process data from preloaded files
function processData() {
    var i;
    var imageFile;
    
    // Title images
    for (i = 0; i < fileData[0].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[0].name + i);
        titleImage[i] = new createjs.Bitmap(imageFile);
    } 
    
    // Icon images
    for(i = 0; i < fileData[1].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[1].name + i);
        iconImage[i] = new createjs.Bitmap(imageFile);
        iconImage[i].id = i;
        
        iconImage[i].x = iconX[i];
        iconImage[i].y = iconY[i];
    }
    
    // Tablet images
    for (i = 0; i < fileData[2].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[2].name + i);
        tabletImage[i] = new createjs.Bitmap(imageFile); 
        tabletImage[i].id = i;
    }
    
    // Map images
    for (i = 0; i < fileData[3].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[3].name + i);
        mapImage[i] = new createjs.Bitmap(imageFile); 
        mapImage[i].id = i;
    }
    
    // Email images
    for (i = 0; i < fileData[4].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[4].name + i);
        emailImage[i] = new createjs.Bitmap(imageFile); 
        emailImage[i].id = i;
    }
    
    // Browser images
    for (i = 0; i < fileData[5].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[5].name + i);
        browserImage[i] = new createjs.Bitmap(imageFile); 
        browserImage[i].id = i;
    }
    
    // Notes images
    for (i = 0; i < fileData[6].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[6].name + i);
        notesImage[i] = new createjs.Bitmap(imageFile); 
    }
    
    // Medical records images
    for (i = 0; i < fileData[7].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[7].name + i);
        medicalImage[i] = new createjs.Bitmap(imageFile); 
    }
    
    // Biodata records images
    for (i = 0; i < fileData[8].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[8].name + i);
        biodataImage[i] = new createjs.Bitmap(imageFile); 
    }
    
    // Criminal records images
    for (i = 0; i < fileData[9].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[9].name + i);
        criminalImage[i] = new createjs.Bitmap(imageFile); 
    }
    
    // Research data images
    for (i = 0; i < fileData[10].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[10].name + i);
        researchImage[i] = new createjs.Bitmap(imageFile); 
        researchImage[i].id = i;
    }
    
    // Home images
    for(i = 0; i < fileData[11].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[11].name + i);
        homeImage[i] = new createjs.Bitmap(imageFile);
        homeImage[i].id = i;
    }
    
    // Mom Flashback images
    for (i = 0; i < fileData[12].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[12].name + i);
        momFlashbackImage[i] = new createjs.Bitmap(imageFile);
    }
    
    // Uncle Flashback images
    for (i = 0; i < fileData[13].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[13].name + i);
        uncleFlashbackImage[i] = new createjs.Bitmap(imageFile);
    }
    
    // Sister Flashback images
    for (i = 0; i < fileData[14].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[14].name + i);
        sisterFlashbackImage[i] = new createjs.Bitmap(imageFile);
    }
    
    // Office images
    for (i = 0; i < fileData[15].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[15].name + i);
        officeImage[i] = new createjs.Bitmap(imageFile);
        officeImage[i].id = i;
    }
    
    // Patient's house A images
    for (i = 0; i < fileData[16].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[16].name + i);
        patientAImage[i] = new createjs.Bitmap(imageFile);
        patientAImage[i].id = i;
    }
  
    // Patient's house B images
    for (i = 0; i < fileData[17].numFile; i++) {
       imageFile = fileQueue.getResult(fileData[17].name + i);
       patientBImage[i] = new createjs.Bitmap(imageFile);
       patientBImage[i].id = i;
    }
    
    // Hospital images
    for (i = 0; i < fileData[18].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[18].name + i);
        hospitalImage[i] = new createjs.Bitmap(imageFile);
        hospitalImage[i].id = i;
    }

    // Library images
    for (i = 0; i < fileData[19].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[19].name + i);
        libraryImage[i] = new createjs.Bitmap(imageFile);
        libraryImage[i].id = i;
    }    

    // City Hall images
    for (i = 0; i < fileData[20].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[20].name + i);
        cityHallImage[i] = new createjs.Bitmap(imageFile);
        cityHallImage[i].id = i;
    }  
    
    // Police Station images
    for (i = 0; i < fileData[21].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[21].name + i);
        policeImage[i] = new createjs.Bitmap(imageFile);
        policeImage[i].id = i;
    }  
    
    // Church images
    for (i = 0; i < fileData[22].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[22].name + i);
        churchImage[i] = new createjs.Bitmap(imageFile);
        churchImage[i].id = i;
    }
 
    // Prison images
    for (i = 0; i < fileData[23].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[23].name + i);
        prisonImage[i] = new createjs.Bitmap(imageFile);
        prisonImage[i].id = i;
    }
 
    // Warehouse images
    for (i = 0; i < fileData[24].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[24].name + i);
        warehouseImage[i] = new createjs.Bitmap(imageFile);
        warehouseImage[i].id = i;
    }
    
    // Aunt's house images
    for (i = 0; i < fileData[25].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[25].name + i);
        auntImage[i] = new createjs.Bitmap(imageFile);
        auntImage[i].id = i;
    }
   
    // Mom's house images
    for (i = 0; i < fileData[26].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[26].name + i);
        momImage[i] = new createjs.Bitmap(imageFile);
        momImage[i].id = i;
    }
   
    // Rehab images
    for (i = 0; i < fileData[27].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[27].name + i);
        rehabImage[i] = new createjs.Bitmap(imageFile);
        rehabImage[i].id = i;
    }  
    
    // Final images
    for (i = 0; i < fileData[28].numFile; i++) {
        imageFile = fileQueue.getResult(fileData[28].name + i);
        finalImage[i] = new createjs.Bitmap(imageFile);
        finalImage[i].id = i;
    }  
    
    // Grey transparent layer behind objects viewed
    greyLayer = new createjs.Shape();
    greyLayer.graphics.beginFill("#111111");
    greyLayer.graphics.drawRect(0,0,1000,700);
    greyLayer.alpha = 0.7;
    
    // Setup and play IF
    resetIF(); 
}