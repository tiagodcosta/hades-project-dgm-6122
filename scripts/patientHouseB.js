function patientHouseB() {
    sceneLocation = "patientHouseB";
    currentBackground = patientBImage[1];
    updateMap(mapImage[4]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(patientBImage[1]);
    scene.addChild(patientBImage[3]);

    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(patientBImage[0], -705, -1440);
}

 function talkPatientBFamily() {
     removeListeners();
     patientBImage[3].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
     rollover.text = "";
     rolloverBox.alpha = 0.5;
    
     scene.addChild(patientBImage[2]);
     patientBImage[2].alpha = 0;
    
     resetSubtitle(8);
     scene.addChild(subtitleBox);
    
     createjs.Ticker.addEventListener("tick", stage); 
     createjs.Tween.get(patientBImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
     createjs.Tween.get(patientBImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
             .call(function() {if (familyBTalked == true) {
                                   patientBImage[1].addEventListener("click", function() {back(false);}, false);
                               } else {
                                   familyBTalked = true;
                               }
                               scene.removeChild(patientBImage[3]);
                               playDialogue("PatientFamilyB");
                               scene.addChild(subtitle);
                               changeSubtitle(0, function() {back(true);});});
    
     function back(done) {
        soundInstance.stop();
        if (done == true && policeAccess == false) {
            policeAccess = true;
            newLocation(mapImage[8]);
            addNote(8);
        }
         
        patientBImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
       
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);

        scene.addChild(patientBImage[3]);
        patientBImage[3].alpha = 0;

        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(patientBImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(patientBImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
         .call(function() {scene.removeChild(patientBImage[2]);
                           rolloverBox.alpha = 0.8;
                           addListeners();
                           createjs.Ticker.removeEventListener("tick", stage);});
     }
 }

function overPatientHouseBItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            rollover.text = "Talk to patient's brother";
            break; 
    }
    addShadow(event, "#222222");
}

function clickPatientHouseBItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 3:
            talkPatientBFamily();
            break; 
    }
}

function addPatientHouseBListeners() {
    for (var i = 3; i < fileData[17].numFile; i++) {
        patientBImage[i].addEventListener("mouseover", overPatientHouseBItem, false);
        patientBImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        patientBImage[i].addEventListener("click", clickPatientHouseBItem, false);
    }
    addIconListeners();
}

function removePatientHouseBListeners() {
    for (var i = 3; i < fileData[17].numFile; i++) {
        patientBImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}