function resetIF() {
    // Set up scene and flasback containers
    scene = new createjs.Container();
    flashback = new createjs.Container();
    
    // Set up rollover and subtitle boxes
    setupTextBoxes();
    
    // Set up tablet and app containers
    setupTablet();

    momFlashWatched = sisFlashWatched = false;
    coworkerATalked = coworkerBTalked = bossTalked = false;
    patientTalked = familyATalked = familyBTalked = false;
    doctorTalked = nurseTalked = hospAdminTalked = false;
    researcherTalked = mayorTalked = cityAdminTalked = false;
    policeTalked = clergyTalked = prisonerATalked = prisonerBTalked = false;
    nemoTalked = auntTalked = momTalked = courtneyTalked = false;

    // Go to title animation
    titleAnim();
}

function setupTextBoxes() {
    var blur = new createjs.BlurFilter(10, 10, 5);
    
    // Set up rollover text boxes
    rolloverBox = new createjs.Shape();
    rolloverBox.graphics.beginFill("#111111");
    rolloverBox.graphics.drawRect(570,30,400,70);
    rolloverBox.alpha = 0.8;

    rolloverBox.filters = [blur];
    rolloverBox.cache(560,20,420,90);
    
    // Set up rollover text
    rollover = new createjs.Text("", "20px Monda", "#AAAAAA");
    rollover.x = 770;
    rollover.y = 65;
    rollover.textAlign = "center";
    rollover.textBaseline = "middle";
    
    // Set up subtitle text boxes
    subtitleBox = new createjs.Shape();
    subtitleBox.graphics.beginFill("#111111");
    subtitleBox.graphics.drawRect(100,615,800,70);
    subtitleBox.alpha = 0.8;

    subtitleBox.filters = [blur];
    subtitleBox.cache(90,605,820,90);
    
    // Set up subtitle text
    subtitle = new createjs.Text("", "20px Monda", "#AAAAAA");
    subtitle.x = 500;
    subtitle.y = 650;
    subtitle.textAlign = "center";
    subtitle.textBaseline = "middle";
}

// Add shadow when mouse is over interactive image
function addShadow(event, color) {
    event.currentTarget.shadow = new createjs.Shadow(color, 0, 0, 10);
    stage.update();
}

// Remove shadow when mouse is out of interactive image
function removeShadow(event) {
    event.currentTarget.shadow = new createjs.Shadow("#000000", 0, 0, 0);
    stage.update();
}

// Exterior to Interior transition
function zoomToInterior(exterior, xFinal, yFinal) {
    exterior.alpha = 0;
    exterior.x = 0;
    exterior.y = 0;
    exterior.scaleX = 1/3.5;
    exterior.scaleY = 1/3.5;
    stage.addChild(exterior);
    
    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(exterior, {loop: false}).to({alpha:1}, 1000, createjs.Ease.quadInOut)
            .call(zoomIn);
    
    function zoomIn() {
        setTimeout(function() {createjs.Ticker.addEventListener("tick", fadeOutSound);}, 1000);
        scene.alpha = 1;
        createjs.Tween.get(exterior, {loop: false}).to({x:xFinal, y:yFinal, scaleX:1, scaleY:1}, 1500, createjs.Ease.quadInOut);
        createjs.Tween.get(exterior, {loop: false}).wait(750).to({alpha:0}, 750, createjs.Ease.quadInOut)
            .call(function () {stage.removeChild(exterior);
                               addListeners();
                               createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function fadeOutSound() {
    soundInstance.volume-=0.02;
    if (soundInstance.volume == 0) {
        soundInstance.stop();
        createjs.Ticker.removeEventListener("tick", fadeOutSound);
    }
}

function fadeInSound() {
    soundInstance.volume+=0.02;
    if (soundInstance.volume == 1) {
        createjs.Ticker.removeEventListener("tick", fadeInSound);
    }
}

function playDialogue(name) {
    soundInstance.stop();
    createjs.Ticker.removeEventListener("tick", fadeOutSound);
    soundInstance = createjs.Sound.play(name);
}

function resetRollover(num) {
    subtitleText = subtitleData[num].text;
    subtitleDelay = subtitleData[num].delay;
    subtitleDuration = subtitleData[num].duration;
    numSubtitle = subtitleText.length;
    
    subtitle = new createjs.Text("", "20px Monda", "#DDDDDD");
    subtitle.x = 500;
    subtitle.y = 650;
    subtitle.textAlign = "center";
    subtitle.textBaseline = "middle";
    subtitle.alpha = 0;
}

function resetSubtitle(num) {
    subtitleText = subtitleData[num].text;
    subtitleDelay = subtitleData[num].delay;
    subtitleDuration = subtitleData[num].duration;
    numSubtitle = subtitleText.length;

    subtitle.alpha = 0;
}

function changeSubtitle(num, callBack) {
        subtitle.text = subtitleText[num];
        createjs.Tween.get(subtitle, {loop: false})
            .wait(subtitleDelay[num]).to({alpha:1}, 200)
            .wait(subtitleDuration[num]).to({alpha:0}, 200)
            .call(function() {if (num == numSubtitle-1) {setTimeout(callBack, subtitleDelay[num+1]);} 
                              else {changeSubtitle(num+1, callBack);}});
}

function overIcon(event) {
    var id = event.currentTarget.id;
    
    switch(id) {
        case 2:
            rollover.text = "Check tablet";
            break;
        case 3:
            rollover.text = "Check map";
            break;
    }
    addShadow(event, "#777777");
}

function clickIcon(event) {
    var id = event.currentTarget.id;
    
    switch(id) {
        case 2:
            currentApp = tablet;
            slideUp();
            break;
        case 3:
            currentApp = map;
            slideUp();
            break;
    }
}

function addIconListeners() {
    for (var i = 2; i < 4; i++) {
        iconImage[i].addEventListener("mouseover", overIcon, false);
        iconImage[i].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
        iconImage[i].addEventListener("click", clickIcon, false);
        iconImage[i].alpha = 1;
    }
    iconImage[0].alpha = 1;
    iconImage[1].alpha = 1;
}

function removeIconListeners() {
    for (var i = 2; i < 4; i++) {
        iconImage[i].removeAllEventListeners();
        iconImage[i].alpha = 0.5;
    }
    iconImage[0].alpha = 0.5;
    iconImage[1].alpha = 0.5;
}

function addListeners() {
    switch(sceneLocation) {
        case "home":
            addHomeListeners();
            break;
        case "office":
            addOfficeListeners();
            break;
        case "patientHouseA":
            addPatientHouseAListeners();
            break;     
        case "patientHouseB":
            addPatientHouseBListeners();
            break;
        case "hospital":
            addHospitalListeners();
            break;
        case "library":
            addLibraryListeners();
            break;
        case "cityHall":
            addCityHallListeners();
            break;
        case "police":
            addPoliceListeners();
            break; 
        case "church":
            addChurchListeners();
            break;   
        case "prison":
            addPrisonListeners();
            break; 
        case "auntHouse":
            addAuntHouseListeners();
            break;
        case "momHouse":
            addMomHouseListeners();
            break; 
        case "rehab":
            addRehabListeners();
            break;
        case "warehouse":
            addWarehouseListeners();
            break;
    }      
}

function removeListeners() {
    switch(sceneLocation) {
        case "home":
            removeHomeListeners();
            break;
        case "office":
            removeOfficeListeners();
            break;
        case "patientHouseA":
            removePatientHouseAListeners(); 
            break;       
        case "patientHouseB":
            removePatientHouseBListeners();
            break; 
        case "hospital":
            removeHospitalListeners();
            break;
        case "library":
            removeLibraryListeners();
            break;
        case "cityHall":
            removeCityHallListeners();
            break;  
        case "police":
            removePoliceListeners();
            break;  
        case "church":
            removeChurchListeners();
            break;  
        case "prison":
            removePrisonListeners();
            break;
        case "auntHouse":
            removeAuntHouseListeners();
            break;
        case "momHouse":
            removeMomHouseListeners();
            break;
        case "rehab":
            removeRehabListeners();
            break;
        case "warehouse":
            removeWarehouseListeners();
            break;
    }    
}