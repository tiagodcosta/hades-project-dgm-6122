function hospital() {
    sceneLocation = "hospital";
    currentBackground = hospitalImage[1];
    updateMap(mapImage[5]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(hospitalImage[1]);
    scene.addChild(hospitalImage[9]);
    scene.addChild(hospitalImage[10]);
    scene.addChild(hospitalImage[11]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(hospitalImage[0], -1280, -1525);
}

function talkDoctor() {
    removeListeners();
    hospitalImage[9].shadow = new createjs.Shadow("#858585", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(hospitalImage[2]);
    hospitalImage[2].alpha = 0;
    
    resetSubtitle(9);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(hospitalImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(hospitalImage[9], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (doctorTalked == true) {
                                  hospitalImage[1].addEventListener("click", function() {back(false);}, false);
                              } else {
                                  doctorTalked = true;
                              }
                              scene.removeChild(hospitalImage[9]);
                              playDialogue("Doctor");
                              scene.addChild(subtitle);
                              changeSubtitle(0, function() {back(true);});});
    
    function back(done) {
        soundInstance.stop();
        if (done == true && libraryAccess == false) {
            libraryAccess = true;
            newLocation(mapImage[6]);
        }
        
        hospitalImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(hospitalImage[9]);
        hospitalImage[9].alpha = 0;
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(hospitalImage[9], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(hospitalImage[2]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function talkNurse() {
    removeListeners();
    hospitalImage[10].shadow = new createjs.Shadow("#858585", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    var cardHanded = false;
    
    scene.addChild(hospitalImage[3]);
    hospitalImage[3].alpha = 0;
    
    resetSubtitle(10);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(hospitalImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(hospitalImage[10], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (nurseTalked == true) {
                                  hospitalImage[1].addEventListener("click", back, false);
                              } else {
                                  nurseTalked = true;
                              }                      
                              scene.removeChild(hospitalImage[10]);
                              playDialogue("Nurse");
                              scene.addChild(subtitle);
                              if (cityHallAccess == true) {
                                  changeSubtitle(0, back);
                              } else {
                                  changeSubtitle(0, handCard);
                              }});
    
    function handCard() {
        cardHanded = true;
        
        scene.addChild(hospitalImage[5]);
        scene.addChild(hospitalImage[12]);
        hospitalImage[5].alpha = 0;
        hospitalImage[12].alpha = 0;
        
        scene.removeChild(subtitleBox);
        
        createjs.Tween.get(hospitalImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[12], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance = createjs.Sound.play("HandObject");
                              scene.removeChild(hospitalImage[3]);
                              rolloverBox.alpha = 0.8;
                              hospitalImage[12].addEventListener("mouseover", overHospitalItem, false);
                              hospitalImage[12].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
                              hospitalImage[12].addEventListener("click", clickHospitalItem, false);
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
    
    function back() {
        hospitalImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(hospitalImage[10]);
        hospitalImage[10].alpha = 0;        
        
        if(cardHanded == true) {
            var num = 5;
            scene.removeChild(hospitalImage[12]);
        } else {
            var num = 3;
        }
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(hospitalImage[10], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[num], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(hospitalImage[num]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function talkHospitalAdmin() {
    removeListeners();
    hospitalImage[11].shadow = new createjs.Shadow("#858585", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(hospitalImage[4]);
    hospitalImage[4].alpha = 0;
    
    var usbHanded = false;
    
    resetSubtitle(11);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(hospitalImage[4], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(hospitalImage[11], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (hospAdminTalked == true) {
                                  hospitalImage[1].addEventListener("click", back, false);
                              } else {
                                  hospAdminTalked = true;
                              }
                              scene.removeChild(hospitalImage[11]);
                              playDialogue("HospitalAdmin");
                              scene.addChild(subtitle);
                              if (medicalAccess == true) {
                                  changeSubtitle(0, back);
                              } else {
                                  changeSubtitle(0, handUSB);
                              }});
    
    function handUSB() {
        usbHanded = true;
        
        scene.addChild(hospitalImage[6]);
        scene.addChild(hospitalImage[13]);
        hospitalImage[6].alpha = 0;
        hospitalImage[13].alpha = 0;
        
        scene.removeChild(subtitleBox);
        
        createjs.Tween.get(hospitalImage[6], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[13], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[4], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance = createjs.Sound.play("HandObject");
                              scene.removeChild(hospitalImage[4]);
                              rolloverBox.alpha = 0.8;
                              hospitalImage[13].addEventListener("mouseover", overHospitalItem, false);
                              hospitalImage[13].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
                              hospitalImage[13].addEventListener("click", clickHospitalItem, false);
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
    
    function back() {
        hospitalImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(hospitalImage[11]);
        hospitalImage[11].alpha = 0;
        
        if(usbHanded == true) {
            var num = 6;
            scene.removeChild(hospitalImage[13]);
        } else {
            var num = 4;
        }
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(hospitalImage[11], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[num], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(hospitalImage[num]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function viewCard() {
    soundInstance = createjs.Sound.play("ViewObject", loopSound);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    hospitalImage[1].removeAllEventListeners();
    hospitalImage[1].addEventListener("click", back, false);
    
    scene.addChild(hospitalImage[10]);
    hospitalImage[10].alpha = 0;
    
    scene.addChild(greyLayer);
    greyLayer.alpha = 0;
    
    scene.addChild(hospitalImage[7]);
    hospitalImage[7].x = 615;
    hospitalImage[7].y = 385;
    hospitalImage[7].scaleX = 1/10;
    hospitalImage[7].scaleY = 1/10;
    
    scene.removeChild(hospitalImage[12]);

    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(hospitalImage[7], {loop: false}).to({x:275, y:200, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(hospitalImage[10], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0.7}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(hospitalImage[5], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
        .call(function() {scene.removeChild(hospitalImage[5]);
                          createjs.Ticker.removeEventListener("tick", stage);});
    
    function back() {
        hospitalImage[1].removeAllEventListeners();
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[7], {loop: false}).to({x:160, y:50, scaleX:1/10, scaleY:1/10}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance.stop();
                              scene.removeChild(greyLayer);
                              scene.removeChild(hospitalImage[7]);
                              cityHallAccess = true;
                              newLocation(mapImage[7]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function viewMedicalUSB() {
    soundInstance = createjs.Sound.play("ViewObject", loopSound);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    hospitalImage[1].removeAllEventListeners();
    hospitalImage[1].addEventListener("click", back, false);
    
    scene.addChild(hospitalImage[11]);
    hospitalImage[11].alpha = 0;
    
    scene.addChild(greyLayer);
    greyLayer.alpha = 0;
    
    scene.addChild(hospitalImage[8]);
    hospitalImage[8].x = 130;
    hospitalImage[8].y = 435;
    hospitalImage[8].scaleX = 1/5;
    hospitalImage[8].scaleY = 1/5;
    
    scene.removeChild(hospitalImage[13]);
    
    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(hospitalImage[8], {loop: false}).to({x:300, y:285, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(hospitalImage[11], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0.7}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(hospitalImage[6], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
        .call(function() {scene.removeChild(hospitalImage[6]);
                          createjs.Ticker.removeEventListener("tick", stage);});
    
    function back() {
        hospitalImage[1].removeAllEventListeners();
        
        createjs.Ticker.addEventListener("tick", stage);
        createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(hospitalImage[8], {loop: false}).to({x:40, y:55, scaleX:1/5, scaleY:1/5}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance.stop();
                              scene.removeChild(greyLayer);
                              scene.removeChild(hospitalImage[8]);
                              medicalAccess = true;
                              newApp(0);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overHospitalItem(event) {
    var id = event.currentTarget.id;
    switch(id) {
        case 9:
            rollover.text = "Ask doctor about HadeS";
            break;
        case 10:
            rollover.text = "Talk to nurse";
            break;
        case 11:
            rollover.text = "Ask admin for patient records";
            break;
        case 12:
            rollover.text = "Take card";
            break;
        case 13:
            rollover.text = "Take flash drive";
            break;
    }
    addShadow(event, "#222222");
}

function clickHospitalItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 9:
            talkDoctor();
            break;
        case 10:
            talkNurse();
            break;
        case 11:
            talkHospitalAdmin();
            break;
        case 12:
            viewCard();
            break;
        case 13:
            viewMedicalUSB();
            break;
    }
}

function addHospitalListeners() {
    for (var i = 9; i < fileData[18].numFile; i++) {
        hospitalImage[i].addEventListener("mouseover", overHospitalItem, false);
        hospitalImage[i].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
        hospitalImage[i].addEventListener("click", clickHospitalItem, false);
    }
    addIconListeners();
}

function removeHospitalListeners() {
    for (var i = 9; i < fileData[18].numFile; i++) {
        hospitalImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}