function rehab() {
    sceneLocation = "rehab";
    currentBackground = rehabImage[1];
    updateMap(mapImage[14]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(rehabImage[1]);
    scene.addChild(rehabImage[5]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(rehabImage[0], -1280, -1080);
}

function talkCourtney() {
    removeListeners();
    rehabImage[5].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    var letterHanded = false;
    
    scene.addChild(rehabImage[2]);
    rehabImage[2].alpha = 0;
    
    resetSubtitle(24);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(rehabImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(rehabImage[5], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (courtneyTalked == true) {
                                  rehabImage[1].addEventListener("click", back, false);
                              } else {
                                  courtneyTalked = true;
                              }
                              scene.removeChild(rehabImage[5]);
                              playDialogue("Courtney");
                              scene.addChild(subtitle);
                              changeSubtitle(0, handLetter);});
    
    function handLetter() {
        letterHanded = true;
        
        scene.addChild(rehabImage[3]);
        scene.addChild(rehabImage[6]);
        rehabImage[3].alpha = 0;
        rehabImage[6].alpha = 0;
        
        scene.removeChild(subtitleBox);
        
        createjs.Tween.get(rehabImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(rehabImage[6], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(rehabImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance = createjs.Sound.play("HandObject");
                              scene.removeChild(rehabImage[2]);
                              rolloverBox.alpha = 0.8;
                              rehabImage[6].addEventListener("mouseover", overRehabItem, false);
                              rehabImage[6].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
                              rehabImage[6].addEventListener("click", clickRehabItem, false);
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
    
    function back() {
        rehabImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(rehabImage[5]);
        rehabImage[5].alpha = 0;
        
        if(letterHanded == true) {
            var num = 3;
            scene.removeChild(rehabImage[6]);
        } else {
            var num = 2;
        }
 
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(rehabImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(rehabImage[num], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {scene.removeChild(rehabImage[num]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function viewLetter() {
    soundInstance = createjs.Sound.play("ViewObject", loopSound);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    rehabImage[1].removeAllEventListeners();
    rehabImage[1].addEventListener("click", back, false);
    
    scene.addChild(rehabImage[5]);
    rehabImage[5].alpha = 0;
    
    scene.addChild(greyLayer);
    greyLayer.alpha = 0;
    
    scene.addChild(rehabImage[4]);
    rehabImage[4].x = 390;
    rehabImage[4].y = 415;
    rehabImage[4].scaleX = 1/10;
    rehabImage[4].scaleY = 1/10;
    
    scene.removeChild(rehabImage[6]);

    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(rehabImage[4], {loop: false}).to({x:85, y:63, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(rehabImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0.7}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(rehabImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
        .call(function() {scene.removeChild(rehabImage[3]);
                          createjs.Ticker.removeEventListener("tick", stage);});
    
    function back() {
        rehabImage[1].removeAllEventListeners();
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(rehabImage[4], {loop: false}).to({x:430, y:700, scaleX:1/10, scaleY:1/10}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance.stop();
                              scene.removeChild(greyLayer);
                              scene.removeChild(rehabImage[4]);
                              setTimeout(function() {newEmail(3);}, emailDelay);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overRehabItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 5:
            rollover.text = "Talk to Courtney";
            break; 
        case 6:
            rollover.text = "Read letter";
            break; 
    }
    addShadow(event, "#222222");
}

function clickRehabItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 5:
            talkCourtney();
            break; 
        case 6:
            viewLetter();
            break;
    }
}

function addRehabListeners() {
    for (var i = 5; i < fileData[27].numFile; i++) {
        rehabImage[i].addEventListener("mouseover", overRehabItem, false);
        rehabImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        rehabImage[i].addEventListener("click", clickRehabItem, false);
    }
    addIconListeners();
}

function removeRehabListeners() {
    for (var i = 5; i < fileData[27].numFile; i++) {
        rehabImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}