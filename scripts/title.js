function titleAnim() {
    stage.removeAllChildren();
    
    soundInstance.stop();
    soundInstance = createjs.Sound.play("Title", loopSound);
    soundInstance.volume = 0;
    createjs.Ticker.addEventListener("tick", fadeInSound);
    
    var fire = titleImage[0];
    var mask = titleImage[1];
    var pattern = titleImage[2];
    var button = titleImage[3];
    
    button.removeAllEventListeners();
    button.addEventListener("mouseover", function(event) {addShadow(event, "#f55b29");}, false);
    button.addEventListener("mouseout", removeShadow, false);
    button.addEventListener("click", startIF, false);
    
    stage.addChild(fire);
    fire.alpha = 0;
    
    stage.addChild(mask);
    mask.x = 0;
    mask.y = 0;
    mask.scaleX = 1/5;
    mask.scaleY = 1/5;
    
    stage.addChild(pattern);
    pattern.x = 0;
    pattern.y = 0;
    pattern.scaleX = 1/5;
    pattern.scaleY = 1/5;
    
    stage.addChild(button);
    button.alpha = 0;
    
    // Delete - only for testing
//    mask.addEventListener("click", shortcut, false);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(fire, {loop: false}).wait(500).to({alpha:1}, 5000, createjs.Ease.quadInOut);
    createjs.Tween.get(mask, {loop: false}).wait(500).to({x:-2000, y:-1400, scaleX:1, scaleY:1}, 7000, createjs.Ease.quadInOut);
    createjs.Tween.get(button, {loop: false}).wait(6000).to({alpha:1}, 1000, createjs.Ease.quadInOut);
    createjs.Tween.get(pattern, {loop: false}).wait(500)
        .to({x:-2000, y:-1400, scaleX:1, scaleY:1, alpha:0}, 7000, createjs.Ease.quadInOut)
        .call(throb);
    
    function throb() {
        createjs.Tween.get(mask, {loop: false})
            .to({x:-1750, y:-1225, scaleX:9/10, scaleY:9/10}, 3000, createjs.Ease.quadInOut)
            .to({x:-2000, y:-1400, scaleX:1, scaleY:1}, 2000, createjs.Ease.quadInOut)
            .call(throb);
    }
    
    function startIF() {
        createjs.Tween.removeAllTweens();
        createjs.Ticker.addEventListener("tick", fadeOutSound);
        
        soundInstanceBG = createjs.Sound.play("Flashback", {volume:0.3});
        
        stage.removeAllChildren();
        stage.addChild(flashback);
        flashback.alpha = 0;

        flashback.addChild(uncleFlashbackImage[1]);
        uncleFlashbackImage[1].alpha = 1;
        uncleFlashbackImage[1].x = 0;
        uncleFlashbackImage[1].y = 0;
        uncleFlashbackImage[1].scaleX = 1/4;
        uncleFlashbackImage[1].scaleY = 1/4;

        flashback.addChild(uncleFlashbackImage[2]);

        resetSubtitle(1);

        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(flashback, {loop: false}).to({alpha:1}, 2000, createjs.Ease.quadInOut)
                .call(playFlashback);

        function playFlashback() {
            flashback.addChild(subtitleBox);
            flashback.addChild(subtitle);
            changeSubtitle(0, function() {flashback.removeChild(subtitle); flashback.removeChild(subtitleBox);});
            soundInstance.stop();
            soundInstance = createjs.Sound.play("UncleFlashback");

            createjs.Tween.get(uncleFlashbackImage[1], {loop: false}).to({x:-435, y:-785, scaleX:0.7, scaleY:0.7}, 5000, createjs.Ease.quadInOut).to({x:-1790, y:-1080}, 6000, createjs.Ease.quadInOut).to({x:0, y:0, scaleX:1/4, scaleY:1/4}, 4000, createjs.Ease.quadInOut)
                .call(back);
        }

        function back() {
            soundInstance.stop();

            createjs.Tween.get(flashback, {loop: false}).to({alpha:0}, 2000, createjs.Ease.quadInOut)
                .call(function() {soundInstanceBG.stop();
                                  stage.removeChild(flashback);
                                  createjs.Ticker.removeEventListener("tick", stage);
                                  soundInstance = createjs.Sound.play("SceneTransition");
                                  home();});
        }
    }
    
    // Delete - only for testing
    function shortcut() {
        createjs.Tween.removeAllTweens();
        createjs.Ticker.addEventListener("tick", fadeOutSound);
        home();
    }
}