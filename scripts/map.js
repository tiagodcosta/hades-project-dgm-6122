function setupMap() {
    patientAccess = hospitalAccess = libraryAccess = cityHallAccess = false;
    policeAccess = churchAccess = prisonAccess = false;
    familyAccess = rehabAccess = false;
    
    mapImage[0].addEventListener("click", function() {}, false);
    
    for (var i = 1; i < fileData[3].numFile; i++) {
        mapImage[i].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
        mapImage[i].addEventListener("mouseout", removeShadow, false);
        mapImage[i].addEventListener("click", clickMap, false);
    }
    
    map = new createjs.Container();
    
    map.addChild(mapImage[0]);
    map.addChild(mapImage[1]); // Home
    map.addChild(mapImage[2]); // Office
    
    // To be deleted - only for testing
//    map.addChild(mapImage[3]); // Patient's house A
//    map.addChild(mapImage[4]); // Patient's house B
//    map.addChild(mapImage[5]); // Hospital
//    map.addChild(mapImage[6]); // Library
//    map.addChild(mapImage[7]); // City Hall
//    map.addChild(mapImage[8]); // Police
//    map.addChild(mapImage[9]); // Church
//    map.addChild(mapImage[10]); // Prison
//    map.addChild(mapImage[11]); // Warehouse
//    map.addChild(mapImage[12]); // Aunt's house
//    map.addChild(mapImage[13]); // Mom's house
//    map.addChild(mapImage[14]); // Rehab
    
    currentMapImage = mapImage[1];
}

function updateMap(image) {
    var prevMapImage = currentMapImage;
    
    prevMapImage.filters = [filterNone];
    prevMapImage.cache(0,0,1000,700);
    prevMapImage.addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
    prevMapImage.addEventListener("mouseout", removeShadow, false);
    prevMapImage.addEventListener("click", clickMap, false);
    
    currentMapImage = image;
    
    currentMapImage.shadow = new createjs.Shadow("#000000", 0, 0, 0);
    currentMapImage.filters = [filterMapCurrent];
    currentMapImage.cache(0,0,1000,700);
    currentMapImage.removeAllEventListeners();
}

function newLocation(image) {
    if (image != mapImage[4]) {
        soundInstance = createjs.Sound.play("LocationUnlocked");
    }
    
    tablet.addChild(tabletImage[1]);
    scene.addChild(iconImage[1]);
    iconImage[1].alpha = 0.5;
    mapNotification = true;
    
    map.addChild(image);
    image.filters = [filterMapNew];
    image.cache(0,0,1000,700);
}

function clickMap(event) {
    var id = event.currentTarget.id;

    soundInstance = createjs.Sound.play("SceneTransition");
    
    switch(id) {
        case 1:
            home();
            break;
        case 2:
            office();
            break;
        case 3:
            patientHouseA();
            break;
        case 4:
            patientHouseB();
            break;
        case 5:
            hospital();
            break;
        case 6:
            library();
            break;
        case 7:
            cityHall();
            break;
        case 8:
            police();
            break;
        case 9:
            church();
            break;
        case 10:
            prison();
            break;
        case 11:
            warehouse();
            break;
        case 12:
            auntHouse();
            break;
        case 13:
            momHouse();
            break;
        case 14:
            rehab();
            break;
    }
}