function library() {
    sceneLocation = "library";
    currentBackground = libraryImage[1];
    updateMap(mapImage[6]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(libraryImage[1]);
    scene.addChild(libraryImage[5]);

    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(libraryImage[0], -1250, -680);
}

function talkResearcher() {
    removeListeners();
    libraryImage[5].shadow = new createjs.Shadow("#000000", 0, 0, 0);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    scene.addChild(libraryImage[2]);
    libraryImage[2].alpha = 0;
    
    var usbHanded = false;
    
    resetSubtitle(12);
    scene.addChild(subtitleBox);
    
    createjs.Ticker.addEventListener("tick", stage); 
    createjs.Tween.get(libraryImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(libraryImage[5], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {if (researcherTalked == true) {
                                  libraryImage[1].addEventListener("click", back, false);
                              } else {
                                  researcherTalked = true;
                              }
                              scene.removeChild(libraryImage[5]);
                              playDialogue("Researcher");
                              scene.addChild(subtitle);
                              if (researchAccess == true) {
                                  changeSubtitle(0, back);
                              } else {
                                  changeSubtitle(0, handUSB);
                              }});
    
    function handUSB() { 
        usbHanded = true;
        
        scene.addChild(libraryImage[3]);
        scene.addChild(libraryImage[6]);
        libraryImage[3].alpha = 0;
        libraryImage[6].alpha = 0;
        
        scene.removeChild(subtitleBox);
        
        createjs.Tween.get(libraryImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(libraryImage[6], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(libraryImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance = createjs.Sound.play("HandObject");
                              scene.removeChild(libraryImage[2]);
                              rolloverBox.alpha = 0.8;
                              libraryImage[6].addEventListener("mouseover", overLibraryItem, false);
                              libraryImage[6].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
                              libraryImage[6].addEventListener("click", clickLibraryItem, false);
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
    
    function back() {
        libraryImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens()
        soundInstance.stop();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);
        
        scene.addChild(libraryImage[5]);
        libraryImage[5].alpha = 0;
        
        if(usbHanded == true) {
            var num = 3;
            scene.removeChild(libraryImage[6]);
        } else {
            var num = 2;
        }
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(libraryImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(libraryImage[num], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
            .call(function() {

                              scene.removeChild(libraryImage[num]);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function viewResearchUSB() {
    soundInstance = createjs.Sound.play("ViewObject", loopSound);
    
    rollover.text = "";
    rolloverBox.alpha = 0.5;
    
    libraryImage[1].removeAllEventListeners();
    libraryImage[1].addEventListener("click", back, false);
    
    scene.addChild(libraryImage[5]);
    libraryImage[5].alpha = 0;
    
    scene.addChild(greyLayer);
    greyLayer.alpha = 0;
    
    scene.addChild(libraryImage[4]);
    libraryImage[4].x = 320;
    libraryImage[4].y = 425;
    libraryImage[4].scaleX = 1/5;
    libraryImage[4].scaleY = 1/5;

    scene.removeChild(libraryImage[6]);
    
    createjs.Ticker.addEventListener("tick", stage); 
    
    createjs.Tween.get(libraryImage[4], {loop: false}).to({x:300, y:285, scaleX:1, scaleY:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(libraryImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0.7}, 500, createjs.Ease.quadInOut);
    createjs.Tween.get(libraryImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
        .call(function() {scene.removeChild(libraryImage[3]);
                          createjs.Ticker.removeEventListener("tick", stage);});
    
    function back() {
        libraryImage[1].removeAllEventListeners();
        
        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(greyLayer, {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(libraryImage[4], {loop: false}).to({x:40, y:55, scaleX:1/5, scaleY:1/5}, 500, createjs.Ease.quadInOut)
            .call(function() {soundInstance.stop();
                              scene.removeChild(greyLayer);
                              scene.removeChild(libraryImage[4]);
                              researchAccess = true;
                              newApp(3);
                              rolloverBox.alpha = 0.8;
                              addListeners();
                              createjs.Ticker.removeEventListener("tick", stage);});
    }
}

function overLibraryItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 5:
            rollover.text = "Ask researcher about journals";
            break;
        case 6:
            rollover.text = "Take flash drive";
            break;    
    }
    addShadow(event, "#222222");
}

function clickLibraryItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 5:
            talkResearcher();
            break;
        case 6:
            viewResearchUSB();
            break;    
    }
}

function addLibraryListeners() {
    for (var i = 5; i < fileData[19].numFile; i++) {
        libraryImage[i].addEventListener("mouseover", overLibraryItem, false);
        libraryImage[i].addEventListener("mouseout", function(event) {rollover.text = ""; removeShadow(event);}, false);
        libraryImage[i].addEventListener("click", clickLibraryItem, false);
    }
    addIconListeners();
}

function removeLibraryListeners() {
    for (var i = 5; i < fileData[19].numFile; i++) {
        libraryImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}