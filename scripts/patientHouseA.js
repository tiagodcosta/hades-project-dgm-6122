function patientHouseA() {
    sceneLocation = "patientHouseA";
    currentBackground = patientAImage[1];
    updateMap(mapImage[3]);
    
    stage.removeAllChildren();
    scene.removeAllChildren();
    
    scene.alpha = 0;
    stage.addChild(scene);
    
    scene.addChild(patientAImage[1]);
    scene.addChild(patientAImage[4]);
    scene.addChild(patientAImage[5]);
    
    scene.addChild(iconImage[2]);
    scene.addChild(iconImage[3]);
    
    rollover.text = "";
    scene.addChild(rolloverBox);
    scene.addChild(rollover);
    
    zoomToInterior(patientAImage[0], -1170, -1560);
}

 function talkPatientA () {
     removeListeners();
     patientAImage[4].shadow = new createjs.Shadow("#858585", 0, 0, 0);
    
     rollover.text = "";
     rolloverBox.alpha = 0.5;
    
     scene.addChild(patientAImage[2]);
     patientAImage[2].alpha = 0;
    
     resetSubtitle(6);
     scene.addChild(subtitleBox);
    
     createjs.Ticker.addEventListener("tick", stage); 
     createjs.Tween.get(patientAImage[2], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
     createjs.Tween.get(patientAImage[4], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
             .call(function() {if (patientTalked == true) {
                                   patientAImage[1].addEventListener("click", function() {back(false);}, false);
                               } else {
                                   patientTalked = true;
                               }
                               scene.removeChild(patientAImage[4]);
                               playDialogue("Patient");
                               scene.addChild(subtitle);
                               changeSubtitle(0, function() {back(true);});});
    
     function back(done) {
        soundInstance.stop();
        if (done == true && hospitalAccess == false) {
            hospitalAccess = true;
            newLocation(mapImage[5]);
        }
         
        patientAImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens(); 
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);

        scene.addChild(patientAImage[4]);
        patientAImage[4].alpha = 0;        

        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(patientAImage[4], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(patientAImage[2], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
         .call(function() {scene.removeChild(patientAImage[2]);
                           rolloverBox.alpha = 0.8;
                           addListeners();
                           createjs.Ticker.removeEventListener("tick", stage);});
     }
 }

 function talkPatientAFamily() {
     removeListeners();
     patientAImage[5].shadow = new createjs.Shadow("#858585", 0, 0, 0);
    
     rollover.text = "";
     rolloverBox.alpha = 0.5;
    
     scene.addChild(patientAImage[3]);
     patientAImage[3].alpha = 0;
    
     resetSubtitle(7);
     scene.addChild(subtitleBox);
    
     createjs.Ticker.addEventListener("tick", stage); 
     createjs.Tween.get(patientAImage[3], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
     createjs.Tween.get(patientAImage[5], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
             .call(function() {if (familyATalked == true) {
                                   patientAImage[1].addEventListener("click", function() {back(false);}, false);
                               } else {
                                   familyATalked = true;
                               }
                               scene.removeChild(patientAImage[5]);
                               playDialogue("PatientFamilyA");
                               scene.addChild(subtitle);
                               changeSubtitle(0, function() {back(true);});});
    
     function back(done) {
        soundInstance.stop();
        if (done == true && churchAccess == false) {
            churchAccess = true;
            newLocation(mapImage[9]);
        }
         
        patientAImage[1].removeAllEventListeners();
        createjs.Tween.removeAllTweens();
        scene.removeChild(subtitle);
        scene.removeChild(subtitleBox);

        scene.addChild(patientAImage[5]);
        patientAImage[5].alpha = 0;

        createjs.Ticker.addEventListener("tick", stage); 
        createjs.Tween.get(patientAImage[5], {loop: false}).to({alpha:1}, 500, createjs.Ease.quadInOut);
        createjs.Tween.get(patientAImage[3], {loop: false}).to({alpha:0}, 500, createjs.Ease.quadInOut)
         .call(function() {scene.removeChild(patientAImage[3]);
                           rolloverBox.alpha = 0.8;
                           addListeners();
                           createjs.Ticker.removeEventListener("tick", stage);});
     }
 }


function overPatientHouseAItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 4:
            rollover.text = "Talk to patient";
            break; 
        case 5:
            rollover.text = "Talk to patient's aunt";
            break;
    }
    addShadow(event, "#222222");
}

function clickPatientHouseAItem(event) {
    var id = event.currentTarget.id;

    switch(id) {
        case 4:
            talkPatientA();
            break; 
        case 5:
            talkPatientAFamily();
    }
}

function addPatientHouseAListeners() {
    for (var i = 4; i < fileData[16].numFile; i++) {
        patientAImage[i].addEventListener("mouseover", overPatientHouseAItem, false);
        patientAImage[i].addEventListener("mouseout", function(event) {rollover.text =""; removeShadow(event);}, false);
        patientAImage[i].addEventListener("click", clickPatientHouseAItem, false);
    }
    addIconListeners();
}

function removePatientHouseAListeners() {
    for (var i = 4; i < fileData[16].numFile; i++) {
        patientAImage[i].removeAllEventListeners();
    }
    removeIconListeners();
}