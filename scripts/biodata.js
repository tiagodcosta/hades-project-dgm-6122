function setupBiodata() {
    biodata = new createjs.Container();
    
    biodata.addChild(biodataImage[0]);
    biodata.addChild(biodataImage[1]);
    biodata.addChild(biodataImage[2]);
    
    biodataImage[0].addEventListener("click", function() {}, false);
    
    biodataImage[2].addEventListener("mouseover", function(event) {addShadow(event, "#777777");}, false);
    biodataImage[2].addEventListener("mouseout", removeShadow, false);
    biodataImage[2].addEventListener("click", showBiodataGraphs, false);
}

function showBiodataGraphs() {
    biodata.addChild(biodataImage[3]);
    
    biodata.removeChild(biodataImage[1]);
    biodata.removeChild(biodataImage[2]);
    
    stage.update();
    
    addNote(6);
}